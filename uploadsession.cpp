/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    uploadsession.cpp
/// \date    2022/01/5
/// \author  Iker Salmon San Millan
///
#include "uploadsession.hpp"

#include <cstring>
#include <iostream>
#include <memory>
#include <time.h>
namespace onedriveapi {

UploadSession::UploadSession(const std::shared_ptr<HttpClient>& httpClient,
                             const std::shared_ptr<IJsonValue>& jsonHandler)
    : mHttpClient(httpClient), mJsonHandler(jsonHandler)
{
  std::cout << mJsonHandler->jsonAsString() << std::endl;
  struct tm time;
  std::memset(&time, 0, sizeof(struct tm));
  strptime("2001-11-12 18:31:01", "%Y-%m-%d %H:%M:%S", &time);
  std::memset(&time, 0, sizeof(struct tm));
  strptime("2022-10-27T21:54:16.905Z", "%Y-%m-%dT%H:%M:%S.905Z", &time);
  std::cout << mJsonHandler->jsonAsString() << std::endl;
}
} // namespace onedriveapi
