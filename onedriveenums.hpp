/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    OneDriveEnums.hpp
/// \date    2022/01/04
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_ONEDRIVEENUMS_HPP
#define ONEDRIVEAPI_ONEDRIVEENUMS_HPP
namespace onedriveapi {
enum class DriveType
{
  personal = 0,
  business,
  documentLibrary
};
enum class IdentityType
{
  user = 0,
  group,
  device,
  application
};
enum class QuotaState
{
  normal = 0,
  nearing,
  critical,
  exceeded
};
} // namespace onedriveapi
#endif //  ONEDRIVEAPI_ONEDRIVEENUMS_HPP
