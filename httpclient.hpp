/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    httpclient.hpp
/// \date    2022/01/5
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_HTTPCLIENT_HPP
#define ONEDRIVEAPI_HTTPCLIENT_HPP

#include <functional>
#include <list>
#include <map>
#include <string>
#include <utility>

#include <curl/curl.h>
#include <curl/easy.h>

using std::list;
using std::pair;
using std::string;

namespace onedriveapi {
constexpr int kChunkDataSize = 327680 * 5; //*190
class HttpClient
{
public:
  enum ResponseCode
  {
    Ok = 200,
    Created,
    Accepted,
    BadRequest = 400,
    Unauthorized,
    Forbidden = 403,
    NotFound,
    MethodNotAllowed,
    NotAcceptable,
    Conflict = 409,
    Gone,
    LengthRequired,
    PreconditionFailed,
    RequestEntityTooLarge,
    UnsupportedMediaType = 415,
    RequestedRangeNotSatisfiable,
    UnprocessableEntity = 422,
    TooManyRequests = 429,
    InternalServerError = 500,
    NotImplemented,
    ServiceUnavailable = 503,
    InsufficientStorage = 507,
    BandwidthLimitExceeded = 509
  };
  std::map<ResponseCode, std::string> ErrorStrings = {
   {BadRequest, "Cannot process the request because it is malformed or incorrect"},
   {Unauthorized, "Required authentication information is either missing or not valid for the resource."},
   {Forbidden, "Access is denied to the requested resource. The user might not have enough permission."},
   {NotFound, "The requested resource doesn’t exist."},
   {MethodNotAllowed, "The HTTP method in the request is not allowed on the resource."},
   {NotAcceptable, "This service doesn’t support the format requested in the Accept header."},
   {Conflict, "The current state conflicts with what the request expects. For example, the specified parent folder "
              "might not exist."},
   {Gone, "The requested resource is no longer available at the server."},
   {LengthRequired, "A Content-Length header is required on the request."},
   {PreconditionFailed, " A precondition provided in the request (such as an if-match header) does not match the "
                        "resource's current state."},
   {RequestEntityTooLarge, "The request size exceeds the maximum limit."},
   {UnsupportedMediaType, "The content type of the request is a format that is not supported by the service."},
   {RequestedRangeNotSatisfiable, "The specified byte range is invalid or unavailable."},
   {UnprocessableEntity, "Cannot process the request because it is semantically incorrect."},
   {TooManyRequests, "Client application has been throttled and should not attempt to repeat the request until an "
                     "amount of time has elapsed."},
   {InternalServerError, "There was an internal server error while processing the request."},
   {NotImplemented, "The requested feature isn’t implemented."},
   {ServiceUnavailable, "The service is temporarily unavailable. You may repeat the request after a delay. There may "
                        "be a Retry-After header"},
   {InsufficientStorage, "The maximum storage quota has been reached."},
   {BandwidthLimitExceeded, "Your app has been throttled for exceeding the maximum bandwidth cap. Your app can retry "
                            "the request again after more time has elapsed."}};
  struct chunk
  {
    const char* data;
    int chunkSize;
    long fileSize;
    int chunkNumber;
  };
  HttpClient();
  const long httpGet(const string& url, const list<pair<string, string>>& headers, string& response);
  const long
  httpPost(const string& url, const list<pair<string, string>>& headers, const string& params, string& response);

  const long downloadFile(const std::string& url, const list<pair<string, string>>& headers, const std::string& path);
  const long uploadFile(const std::string& url,
                        const list<pair<string, string>>& headers,
                        const std::string& path,
                        const long fileSize,
                        string& response);
  const long uploadChunk(const std::string& url,
                         const list<pair<string, string>>& headers,
                         struct chunk* dataChunk,
                         const char* data,
                         const long chunkSize,
                         const long fileSize,
                         string& response);

  const long deleteItem(const std::string& url, const list<pair<string, string>>& headers, string& response);
  const std::string urlEncode(const std::string& str);
  void setVerbose(const bool& vervose);
  void setProgressObserverCallback(std::function<void(int)> function) { mProgressNotifier = function; }

private:
  static size_t writeResponseCallback(char* ptr, size_t size, size_t nmemb, void* userp);
  static int
  download_progress_callback(void* clientp, curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow);
  static size_t read_callback(char* buffer, size_t size, size_t nitems, void* userdata);

private:
  bool mVerbose = false;
  string mResponse;
  std::function<void(int)> mProgressNotifier;
};
} // namespace onedriveapi
#endif // ONEDRIVEAPI_HTTPCLIENT_HPP
