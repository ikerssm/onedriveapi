/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    uploadsession.hpp
/// \date    2022/01/5
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_UPLOADSESSION_HPP
#define ONEDRIVEAPI_UPLOADSESSION_HPP

#include <ctime>
#include <memory>
#include <string>

#include "httpclient.hpp"
#include "interfaces/ijsonvalue.hpp"

namespace onedriveapi {
class UploadSession
{
public:
  UploadSession(const std::shared_ptr<HttpClient>& httpClient, const std::shared_ptr<IJsonValue>& jsonHandler);

private:
  std::shared_ptr<IJsonValue> mJsonHandler;
  std::string mUploadUrl;
  std::time_t mExpiresIn = 0;
  std::string mLocalFile;
  std::shared_ptr<HttpClient> mHttpClient;
};
} // namespace onedriveapi
#endif // ONEDRIVEAPI_UPLOADSESSION_HPP
