/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    jsonparserinterface.hpp
/// \date    2022/01/5
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_JSONHANDLERINTERFACE_HPP
#define ONEDRIVEAPI_JSONHANDLERINTERFACE_HPP

#include "../onedriveapi_global.hpp"

#include <memory>
#include <string>
#include <vector>

namespace onedriveapi {

class ONEDRIVEAPI_EXPORT IJsonValue
{
public:
  virtual const std::string getKey(const std::string& key) = 0;
  virtual const long getKeyAsLong(const std::string& key) = 0;
  virtual const bool getKeyAsBool(const std::string& key) = 0;
  virtual const bool getKeyAsDouble(const std::string& key) = 0;
  ///
  /// \brief getObject get a json object from the json
  /// \param key key of the object to retreive
  /// \return must return null if the object does not exists
  ///
  virtual const std::shared_ptr<IJsonValue> getObject(const std::string& key) = 0;
  virtual const std::vector<std::shared_ptr<IJsonValue>> getObjects(const std::string& key) = 0;
  virtual bool parseJson(const std::string& jsonString) = 0;
  virtual const bool hasTokens() { return !getKey("access_token").empty(); }
  virtual const std::string& jsonAsString() = 0;
  virtual const std::vector<std::string> getKeys() = 0;

protected:
  std::string mJsonString;
};
} // namespace onedriveapi
#endif // ONEDRIVEAPI_JSONHANDLERINTERFACE_HPP
