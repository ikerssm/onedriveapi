/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    ioddriveitem.hpp
/// \date    2022/5/26
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_IODDRIVEITEM_HPP
#define ONEDRIVEAPI_IODDRIVEITEM_HPP
#include "../onedriveapi_global.hpp"
#include "ijsonvalue.hpp"
#include "iodbaseitem.hpp"

namespace onedriveapi {

class ONEDRIVEAPI_EXPORT IODDriveItem : public IODBaseItem
{
public:
  virtual bool setDriveItemProperties(IJsonValue* data) = 0;
  virtual void printItem() = 0;
  virtual bool addChildren(IJsonValue* data) = 0;
};
} // namespace onedriveapi
#endif // ONEDRIVEAPI_IODDRIVEITEM_HPP
