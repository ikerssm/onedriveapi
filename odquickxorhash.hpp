/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    odquickxorhash.hpp
/// \date    2022/01/26
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_ODQUICKXORHASH_HPP
#define ONEDRIVEAPI_ODQUICKXORHASH_HPP

#include "onedriveapi_global.hpp"

#include <stdint.h>
#include <string>

namespace onedriveapi {
static const size_t KBlockSize = 160;
static const size_t KCellBits = KBlockSize % 64;
static const size_t KShift = 11;
static const size_t KBase64EncodedLength = 29;
static const char KTrailingChar = '=';
static const char* KBase64Chars = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                   "abcdefghijklmnopqrstuvwxyz"
                                   "0123456789"
                                   "+/"};
class ONEDRIVEAPI_EXPORT ODQuickXorHash
{
public:
  ODQuickXorHash();
  void quickXor(char* data, size_t length);
  bool readFile(const std::string& filename);
  std::string base64Hash();

  size_t mShifted;
  size_t mPosition;
  size_t mCellSize = 3;
  uint64_t mCell[3] = {0, 0, 0};
  char mHash[20] = {0};
};

} // namespace onedriveapi
#endif // ONEDRIVEAPI_ODQUICKXORHASH_HPP
