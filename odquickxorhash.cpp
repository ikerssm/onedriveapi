/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    odquickxorhash.cpp
/// \date    2022/11/26
/// \author  Iker Salmon San Millan
///
#include "odquickxorhash.hpp"
#include <fstream>
#include <iostream>
#include <istream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "odquickxorhash.hpp"
namespace onedriveapi {

ODQuickXorHash::ODQuickXorHash() : mShifted(0), mPosition(0) {}

void ODQuickXorHash::quickXor(char* data, size_t length)
{
  size_t cellPosition = mShifted / 64;
  size_t bitPosition = mShifted % 64;

  for (size_t i = 0; i < KBlockSize; i++)
  {
    uint8_t nByte = 0x0;
    for (size_t j = i; j < length; j += KBlockSize)
    {
      nByte ^= data[i];
    }
    mCell[cellPosition] ^= uint64_t(nByte) << bitPosition;

    size_t nextPosition = cellPosition + 1;
    size_t remainingBits = 64;
    if (nextPosition == mCellSize)
    {
      nextPosition = 0;
      if (KBlockSize % 64 > 0)
      {
        remainingBits = KBlockSize % 64;
      }
    }
    if (bitPosition > remainingBits - 8)
    {
      mCell[nextPosition] ^= uint64_t(nByte) >> (remainingBits - bitPosition);
    }

    bitPosition += KShift;
    if (bitPosition >= remainingBits)
    {
      cellPosition = nextPosition;
      bitPosition -= remainingBits;
    }
  }
  mShifted += KShift * (length % KBlockSize);
  mShifted %= KBlockSize;
  mPosition += length;
}

bool ODQuickXorHash::readFile(const std::string& filename)
{
  std::ifstream input(filename.c_str(), std::ios::binary);
  auto result = input.is_open();
  if (result)
  {
    char inputData[KBlockSize];

    while (!input.eof())
    {
      input.read(&inputData[0], KBlockSize);
      quickXor(inputData, input.gcount());
    }
    memcpy(mHash, mCell, sizeof(char) * 20);
    char positionV[8];
    memcpy(positionV, &mPosition, 8);
    for (int i = 0; i < 8; i++)
    {
      mHash[KBlockSize / 8 - 8 + i] ^= positionV[i];
    }
    input.close();
  }
  return result;
}

std::string ODQuickXorHash::base64Hash()
{
  std::string ret;
  ret.reserve(KBase64EncodedLength);

  unsigned int pos = 0;
  while (pos < 20)
  {
    ret.push_back(KBase64Chars[(mHash[pos + 0] & 0xfc) >> 2]);

    if (pos + 1 < 20)
    {
      ret.push_back(KBase64Chars[((mHash[pos + 0] & 0x03) << 4) + ((mHash[pos + 1] & 0xf0) >> 4)]);

      if (pos + 2 < 20)
      {
        ret.push_back(KBase64Chars[((mHash[pos + 1] & 0x0f) << 2) + ((mHash[pos + 2] & 0xc0) >> 6)]);
        ret.push_back(KBase64Chars[mHash[pos + 2] & 0x3f]);
      }
      else
      {
        ret.push_back(KBase64Chars[(mHash[pos + 1] & 0x0f) << 2]);
        ret.push_back(KTrailingChar);
      }
    }
    else
    {
      ret.push_back(KBase64Chars[(mHash[pos + 0] & 0x03) << 4]);
      ret.push_back(KTrailingChar);
      ret.push_back(KTrailingChar);
    }

    pos += 3;
  }

  return ret;
}

} // namespace onedriveapi
