/// OneDriveApi
/// Copyright (C) 2021, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    odsharepointids.cpp
/// \date    2021/11/2
/// \author  Iker Salmon San Millan
///
#include "odsharepointids.hpp"
namespace onedriveapi {

ODSharepointIds::ODSharepointIds() {}

const string& ODSharepointIds::listId() const
{
  return mListId;
}

void ODSharepointIds::setListId(const string& newListId)
{
  mListId = newListId;
}

const string& ODSharepointIds::listItemId() const
{
  return mListItemId;
}

void ODSharepointIds::setListItemId(const string& newListItemId)
{
  mListItemId = newListItemId;
}

const string& ODSharepointIds::listIntemUniqueId() const
{
  return mListIntemUniqueId;
}

void ODSharepointIds::setListIntemUniqueId(const string& newListIntemUniqueId)
{
  mListIntemUniqueId = newListIntemUniqueId;
}

const string& ODSharepointIds::siteId() const
{
  return mSiteId;
}

void ODSharepointIds::setSiteId(const string& newSiteId)
{
  mSiteId = newSiteId;
}

const string& ODSharepointIds::siteUrl() const
{
  return mSiteUrl;
}

void ODSharepointIds::setSiteUrl(const string& newSiteUrl)
{
  mSiteUrl = newSiteUrl;
}

const string& ODSharepointIds::tenantId() const
{
  return mTenantId;
}

void ODSharepointIds::setTenantId(const string& newTenantId)
{
  mTenantId = newTenantId;
}
} // namespace onedriveapi
