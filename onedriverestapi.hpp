/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    onedriveapi.hpp
/// \date    2022/01/7
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_ONEDRIVERESTAPI_HPP
#define ONEDRIVEAPI_ONEDRIVERESTAPI_HPP

#include "httpclient.hpp"
#include "interfaces/ijsonvalue.hpp"
#include "onedriveapi_global.hpp"

#include <ctime>
#include <memory>
#include <string>
#include <vector>

namespace onedriveapi {

class ONEDRIVEAPI_EXPORT OneDriveRestApi
{
public:
  template<typename... Args>
  std::string string_format(const char* fmt, Args... args)
  {
    size_t size = snprintf(nullptr, 0, fmt, args...);
    std::string buf;
    buf.reserve(size + 1);
    buf.resize(size);
    snprintf(&buf[0], size + 1, fmt, args...);
    return buf;
  }
  enum AuthorizationType
  {
    Code = 0,
    RefreshToken
  };

  using JsonHandlerPtr = std::shared_ptr<IJsonValue>;

  OneDriveRestApi();
  ///
  /// \brief setScope set authentication scopes read more:
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/getting-started/graph-oauth?view=odsp-graph-online
  /// \param newScope string with the scope.  (default=
  /// "Files.ReadWrite%20Files.ReadWrite.all%20Sites.Read.All%20Sites.ReadWrite.All%20offline_access")
  ///
  void setScope(const std::string& newScope);
  ///
  /// \brief setAuthEndpoint set Azure Active Directory login endpoint. default is the global one:
  /// https://login.microsoftonline.com \param newAuthEndpoint could be for example US:
  /// https://login.microsoftonline.us, German: https://login.microsoftonline.de, Spain:
  /// https://login.microsoftonline.es
  ///
  void setAuthEndpoint(const std::string& newAuthEndpoint);
  ///
  /// \brief setAuthRedirectUrl set the redirect url for authentification purposes
  /// \param newAuthRedirectUrl new redirect url
  ///
  void setAuthRedirectUrl(const std::string& newAuthRedirectUrl);
  ///
  /// \brief setTokenUrl set athentication token
  /// \param newTokenUrl new token
  ///
  void setTokenUrl(const std::string& newTokenUrl);
  ///
  /// \brief setClientID set client ID
  /// \param newClientID new client ID
  ///
  void setClientID(const std::string& newClientID);

  ///
  /// \brief authorize obtain tokens with code or refresh token as in the code flow shown in
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/getting-started/graph-oauth?view=odsp-graph-online
  /// \param type Type for authentication Code=0, refreshToken=1
  /// \param value  token or code for authentication
  /// \return a string with microsoft response for the request
  ///
  const std::string& authorize(OneDriveRestApi::AuthorizationType type, const std::string& value);

  ///
  /// \brief getLoginUrl get the url where the user must authenticate to obtain the code for retrieving tokens
  /// \return authentication url
  ///
  const std::string getLoginUrl();

  ///
  /// \brief setVerbose for debug purposes, turn on or off verbosity of http requests
  /// \param verbose true to activate verbosity
  ///
  void setVerbose(bool verbose);

  ///
  /// \brief setAccesToken manually set the accesToken
  /// \param newAccesToken the accesToken
  ///
  void setAccesToken(const std::string& newAccesToken);

  ///
  /// \brief getAccessToken get the Acces Token
  /// \return the access Token
  ///
  const std::string& getAccessToken() const;

  /// \brief tokenStillValid to check if the acces token has expired
  /// \return true if the token has not yet expired
  bool tokenStillValid();

  ///
  /// \brief setProgressObserver pass a progress Observer callback function for downloading and uploading files.
  /// The function will be called with the percentaje progress in the first parameter.
  /// \param function a void function(int)
  ///
  void setProgressObserver(std::function<void(int)> function);

  ///
  /// \brief getDrive https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/drive_get
  /// \return a string with microsoft response for the request;
  ///
  const std::string& getDrive();

  ///
  /// \brief getRoot https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_get
  /// \return a string with microsoft response for the request;
  ///
  const std::string& getRoot();

  ///
  /// \brief getRootOfDriveID https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_get
  /// \param driveID the drive ID
  /// \return a string with microsoft response for the request;
  ///
  const std::string& getRootOfDriveID(const std::string& driveID);

  ///
  /// \brief getSharedWithMe  https://docs.microsoft.com/en-us/graph/api/drive-sharedwithme
  /// \return a string with microsoft response for the request;
  ///
  const std::string& getSharedWithMe();

  ///
  /// \brief listChildren list childreen of an item:
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_list_children
  /// \param driveId the DriveId of the Item
  /// \param itemID the Item ID
  /// \param nextLink url containing the next "page" of changes
  /// \return a string with microsoft response for the request
  ///
  const std::string& listChildren(const string& driveId, const string& itemID, const string& nextLink = std::string());

  ///
  /// \brief listDriveChanges // https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_delta
  /// \param driveID the Drive ID
  /// \param nextLink next page of changes  (pass it empty for the first search, or last delta for changes since last
  /// query)
  /// \return a string with microsoft response for the request
  ///
  const std::string& listDriveChanges(const string& driveID, const string& nextLink = std::string());

  ///
  /// \brief listItemChanges// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_delta
  /// \param driveId the Drive ID
  /// \param itemID the Item ID
  /// \param nextLink nextLink next page of changes (pass it empty for the first search)
  /// \return a string with microsoft response for the request
  ///
  const std::string&
  listItemChanges(const string& driveId, const string& itemID, const string& nextLink = std::string());

  ///
  /// \brief downloadById https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_get_content
  /// \param driveIdthe Drive ID
  /// \param itemID the Item ID
  /// \param saveToPath path to save the file
  /// \return true if succedeed
  ///
  bool downloadById(const string& driveId, const string& itemID, const string& saveToPath);

  ///
  /// \brief createUploadSession as in
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_createuploadsession
  /// \param parentDrive  drive id  to upload
  /// \param parentItem parent item id
  /// \param filename target filename in one drive
  /// \param jsonBody optional string containing a json with extradata as described in the rest api doc
  /// \return a string with microsoft response for the request
  ///
  const std::string& createUploadSession(const string& parentDrive,
                                         const string& parentItem,
                                         const string& filename,
                                         const std::string& jsonBody = std::string());
  ///
  /// \brief uploadFile upload a file less than 4Mb to onedrive:
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_put_content
  /// \param parentDrive drive to upload
  /// \param parentItem parent item
  /// \param filename target filename in one drive
  /// \param localpath local file
  /// \return a string with microsoft response for the request
  ///
  const std::string& uploadSmallFile(const string& parentDrive,
                                     const string& parentItem,
                                     const string& filename,
                                     const string& localpath,
                                     const long fileSize);

  ///
  /// \brief uploadchunk as in
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_createuploadsession \param url
  /// uploadUrl of the session
  /// \param data the chunk data
  /// \param initialByte initial byte of the file corresponding to the first byte of the data
  /// \param bytes  the chunksize must be a multiple o 367680(bytes) and smaller than 60MiB
  /// \param filesize the filesize
  /// \return returns true if
  /// succedeed
  ///
  bool uploadchunk(const string& url, const char* data, const long initialByte, const long bytes, const long filesize);

  ///
  /// \brief deleteItem as in https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_delete
  /// \param driveId the drive id
  /// \param itemID the item id
  /// \param eTag to match the eTag for deletion
  /// \return  a string with microsoft response for the request
  ///
  const std::string& deleteItem(const string& driveId, const string& itemID, const std::string& eTag = std::string());

  ///
  /// \brief findAllSites returns all sites from the sharepoint.  as in:
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/site_search
  /// \param nextUrl for long searches whenresults are more than 200
  /// \return a string with microsoft response for the request
  ///
  const std::string& findAllSites(std::string& nextUrl);

  ///
  /// \brief getSiteDrives get the drives of a site
  /// \param siteId the Site Id
  /// \return  true if succedded
  ///
  const std::string& getSiteDrives(const std::string& siteId);

  ///
  /// \brief searchItemInSite search for an item in a site
  /// \param siteId the id of the site to search on
  /// \param searchFor string search in the site
  /// \return true if succedded
  ///
  bool searchItemInSite(const std::string& siteId, const std::string& searchFor);

  bool genericHttpPost();

  ///
  /// \brief response
  /// \return returns the response json as string
  ///
  const std::string& response() const;

private:
  list<pair<string, string>> getAuthorizationHeader();

private:
  std::shared_ptr<IJsonValue> mJsonHandler;
  std::shared_ptr<HttpClient> mHttpClient;
  std::string mAccessToken;
  std::string mClientID = "4d289b42-9819-4d18-9904-912d907d04b4";
  std::string mScope = "Files.ReadWrite%20Files.ReadWrite.all%20Sites.Read.All%20Sites.ReadWrite.All%20offline_access";
  std::string mAuthEndpoint = "https://login.microsoftonline.com/common/oauth2/v2.0/authorize";
  std::string mGraphEndPoit = "https://graph.microsoft.com/v1.0/";
  std::string mAuthRedirectUrl = "https://login.microsoftonline.com/common/oauth2/nativeclient";
  std::string mTokenUrl = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
  std::string mResponse;
};
} // namespace onedriveapi
#endif // ONEDRIVERESTAPI_HPP
