# OneDriveApi

Simple OneDrive Api written in c++


## Description
An api for developing applications for oneDrive in linux. Tha main goal is to develop something that can be usable with Qt but also independet enough so it can be used anyhow.
Curl is a must for dependencies and jsoncpp is optional. (You can implement your own jsonhandler interface if you want as it is my plan with Qt but I implemented a basic one with jsoncpp so the api is functional without Qt) 

## Table of Contents 

- [Installation](#installation) 
- [Usage](#usage) 
- [Contributing](#contributing) 
- [TODO](#TODO) 
- [License](#license) 

## Installation
Just standard stuff. If this gets more complicated I will write some instructions

## Usage
TODO

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## TODO
- Better design?
- upload session still pending
- ???

## Contributing
You are wellcome to contribute, suggest changes or whatever.


## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

## Project status
Developing in my free time wich is not much
