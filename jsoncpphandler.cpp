/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    jsoncppparser.cpp
/// \date    2022/01/4
/// \author  Iker Salmon San Millan
///
#include "jsoncpphandler.hpp"
#include "templates.hpp"

#include <iostream>

namespace onedriveapi {

JsonCppHandler::JsonCppHandler() {}

bool JsonCppHandler::parseJson(const std::string& jsonString)
{
  auto result{true};
  Json::CharReaderBuilder rbuilder;
  std::shared_ptr<Json::CharReader> const reader(rbuilder.newCharReader());
  std::string errs;
  reader->parse(jsonString.c_str(), jsonString.c_str() + jsonString.length(), &mValue, &errs);
  if (!errs.empty())
  {
    result = false;
    std::cout << "Error parsing json response: " << std::endl << errs << std::endl;
  }
  mJsonString = jsonString;
  return result;
}

const string JsonCppHandler::getKey(const string& key)
{
  return mValue.get(key, "").asString();
}

const long JsonCppHandler::getKeyAsLong(const string& key)
{
  return mValue.get(key, 0).asLargestInt();
}

const bool JsonCppHandler::getKeyAsBool(const std::string& key)
{
  auto result = false;

  if (contains(mValue.getMemberNames(), key))
  {
    result = mValue.get(key, "").asBool();
  }
  return result;
}

const bool JsonCppHandler::getKeyAsDouble(const std::string& key)
{
  return mValue.get(key, "").asDouble();
}

const std::shared_ptr<IJsonValue> JsonCppHandler::getObject(const string& key)
{
  std::shared_ptr<JsonCppHandler> jsonHandler = nullptr;

  Json::Value object = mValue.get(key, "");
  if (object.isObject())
  {
    jsonHandler = std::make_shared<JsonCppHandler>();
    jsonHandler->setValue(object);
  }
  return jsonHandler;
}

void JsonCppHandler::setValue(const Json::Value& value)
{
  mValue = value;
}

const std::vector<std::shared_ptr<IJsonValue>> JsonCppHandler::getObjects(const string& key)
{
  using namespace std;
  vector<shared_ptr<IJsonValue>> result;
  Json::Value array = mValue.get("value", "");
  if (array.isArray())
  {
    for (const auto& a : array)
    {
      auto jsonHandler = make_shared<JsonCppHandler>();
      jsonHandler->setValue(a);
      result.push_back(jsonHandler);
    }
  }
  return result;
}

const string& JsonCppHandler::jsonAsString()
{
  return mJsonString;
}
} // namespace onedriveapi

const std::vector<std::string> onedriveapi::JsonCppHandler::getKeys()
{
  return mValue.getMemberNames();
}
