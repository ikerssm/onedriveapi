/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    onedriveapi.hpp
/// \date    2022/01/17
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_ONEDRIVEAPI_HPP
#define ONEDRIVEAPI_ONEDRIVEAPI_HPP
#include <array>
#include <memory>
#include <vector>

#include "interfaces/ijsonvalue.hpp"
#include "interfaces/iodcollection.hpp"
#include "interfaces/ioddrive.hpp"
#include "onedriveapi_global.hpp"
#include "onedriverestapi.hpp"
#include "resources/oddrive.hpp"
#include "resources/oddriveitem.hpp"
#include "resources/oddriveitemcollection.hpp"
#include "resources/odsite.hpp"
#include "uploadsession.hpp"

namespace onedriveapi {

class ONEDRIVEAPI_EXPORT OneDriveApi
{
public:
  OneDriveApi();

  ///
  /// \brief getLoginUrl get the url where the user must authenticate to obtain
  /// the code for retrieving tokens
  /// \return authentication url
  ///
  const std::string getLoginUrl();

  ///
  /// \brief authorize obtain tokens with code or refresh token as in the code
  /// flow shown in
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/getting-started/graph-oauth?view=odsp-graph-online
  /// \param type Type for authentication Code=0, refreshToken=1
  /// \param value  token or code for authentication
  /// \return true if autentication succeded false otherwise
  ///
  const bool authorize(OneDriveRestApi::AuthorizationType type, const std::string& value);

  /// \brief tokenStillValid to check if the acces token has expired
  /// \return true if the token has not yet expired
  const bool tokenStillValid();

  ///
  /// \brief setJsonHandler set a JsonHandler necessary for some automatic
  /// functions if compiled without jsoncpp.  It can also be useed if you want
  /// more control handling the Json responses.
  /// \param newJsonHandler a JsonHandlerInterface object
  ///
  void setJsonHandler(const std::shared_ptr<IJsonValue>& newJsonHandler);

  ///
  /// \brief getOneDriveMainDrive get the user main drive and store it in drive
  /// parameter
  /// \param drive ODDrive pointer where the data is stored
  ///  \return true if succedeed
  ///
  const bool getOneDriveMainDrive(IODDrive* drive);

  ///
  /// \brief getRootOfDrive set the root of a Drive
  /// \param drive ODDrive pointer where the data is stored
  /// \return  true if succedeed
  ///
  const bool getRootOfDrive(IODDrive* drive);

  ///
  /// \brief getSharedWithMe
  /// https://docs.microsoft.com/en-us/graph/api/drive-sharedwithme
  /// \param list vector of ODDriveTtems where the items will be stored
  /// \return true if the collection is not empty
  ///
  const bool getSharedWithMe(IODCollection* collection);

  ///
  /// \brief getChildren get the children of a driveItem and store them using
  /// setChildren virtual function
  /// \param item the drive item
  /// \return true if all the children are added (it the item has no children
  /// will return false)
  ///
  const bool getChildren(IODDriveItem* item);

  ///
  /// \brief getDriveChanges  get all the changes of the drive since last call
  /// (lastDelta)
  /// \param drive the drive of the changes.
  /// \param changes A Collection of drive items where changed items will be
  /// stored.
  /// \param newDelta  url with the delta for the next search so the
  /// changes shown are only the new ones.
  /// \param lastDelta url with the deltaof the last search.If not setted all
  /// the changes since the creation of the drive shall be stored
  /// \return true if succedeed
  ///
  const bool getDriveChanges(IODDrive* drive,
                             IODCollection* changes,
                             std::string& newDelta,
                             const std::string& lastDelta = std::string());

  ///
  /// \brief getItemChanges  get all the changes of an item since last call
  /// (lastDelta)
  /// \param drive Drive that contains the item
  /// \param item the item with the changes
  /// \param changes A Collection of drive items where changed items will be
  /// stored
  /// \param newDelta url with the delta for the next search so the changes
  /// shown are only the new ones
  /// \param lastDelta url with the delta of the last search.  If not setted
  /// all the changes since the creation of the drive shall be stored
  /// \return true if succedeed
  ///
  const bool getItemChanges(IODDrive* drive,
                            IODDriveItem* item,
                            IODCollection* changes,
                            std::string& newDelta,
                            const std::string& lastDelta = std::string());

  ///
  /// \brief downloadFile
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_get_content
  /// \param driveIdthe Drive where the file is stored
  /// \param itemID the Item representing the file
  /// \param saveToPath path to save the file
  /// \return true if succedeed
  ///
  bool downloadFile(IODDrive* drive, IODDriveItem* item, const string& saveToPath);

  ///
  /// \brief downloadFile
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_get_content
  /// \param driveIdthe Drive ID
  /// \param itemID the Item ID
  /// \param saveToPath path to save the file
  /// \return true if succedeed
  ///
  bool downloadFile(const string& driveId, const string& itemID, const string& saveToPath);

  ///
  /// \brief uploadFile upload a file less than 4Mb to onedrive:
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_put_content
  /// \param parentDrive drive to upload
  /// \param parentItem parent item
  /// \param filename target filename in one drive
  /// \param localpath local file
  /// \return true if succedeed
  ///
  bool uploadSmallFile(
   IODDrive* drive, IODDriveItem* item, const string& filename, const string& localpath, const long fileSize);

  ///
  /// \brief uploadFile upload a file less than 4Mb to onedrive:
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_put_content
  /// \param parentDrive drive id to upload
  /// \param parentItem parent item id where the file will be stored
  /// \param filename target filename in one drive
  /// \param localpath local file
  /// \return true if succedeed
  ///
  bool uploadSmallFile(const string& parentDrive,
                       const string& parentItem,
                       const string& filename,
                       const string& localpath,
                       const long fileSize);

  ///
  /// \brief deleteItem as in
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_delete
  /// \param driveId the drive id
  /// \param itemID the item id
  /// \param eTag to match the eTag for deletion
  /// \return  true if succedeed
  ///
  bool deleteItem(IODDrive* drive, IODDriveItem* item, const std::string& eTag = std::string());

  ///
  /// \brief deleteItem as in
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_delete
  /// \param driveId the drive
  /// \param itemID the item
  /// \param eTag to match the eTag for deletion
  /// \return  true if succedeed
  ///
  bool deleteItem(const string& driveId, const string& itemID, const std::string& eTag = std::string());

  ///
  /// \brief findAllSitesID returns all sites id from the sharepoint.  as in:
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/site_search
  /// \return a vector of strings with all the sites in the sharepoint
  ///
  const std::vector<std::string> findAllSitesID();

  ///
  /// \brief findAllSites returns all sites from the sharepoint.  as in:
  /// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/site_search
  /// \return a vector of ODSites with all the sites in the sharepoint
  ///
  const std::vector<ODSite> findAllSites();

  ///
  /// \brief refreshToken get the refresh token obtained after code auth
  /// \return  std:string with the token
  ///
  const std::string& getRefreshToken() const;

  ///
  /// \brief getSiteDrives get drives of sharepoint site
  /// \param siteId the site id
  /// \param drives a iodCollection to store the drives
  ///
  void getSiteDrives(const std::string& siteId, IODCollection* drives);

  ///
  /// \brief getSiteDrives get a vector of the drives in the sharepoint site
  /// \param siteId site id
  /// \return a vector of T* type classes that must implement IODriveInterface
  template<typename T>
  std::vector<T*> getSiteDrives(const std::string& siteId)
  {
    std::vector<T*> drives;
    renewTokenIfNeeded();
    if (mJsonHandler.get())
    {
      if (mJsonHandler->parseJson(mRestApi.getSiteDrives(siteId)))
      {
        auto siteDriveObjects = mJsonHandler->getObjects("value");
        std::for_each(siteDriveObjects.begin(), siteDriveObjects.end(),
                      [&drives](const std::shared_ptr<IJsonValue>& object) {
                        auto drive = new T();
                        drive->setDriveProperties(object.get());
                        drives.push_back(drive);
                      });
      }
    }
    return drives;
  }

private:
  UploadSession createUploadSession(const string& parentDriveID,
                                    const string& parentItemID,
                                    const string& filename,
                                    IJsonValue* optionalBody);

  void renewTokenIfNeeded();
  const bool responseHasNoError(IJsonValue* data);

private:
  OneDriveRestApi mRestApi;
  std::shared_ptr<IJsonValue> mJsonHandler;
  std::string mRefreshToken;
  time_t mTokenExpiresIn = 0;
};
} // namespace onedriveapi
#endif // ONEDRIVEAPI_OONEDRIVEAPI_HPP
