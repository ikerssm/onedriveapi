/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    templates.h
/// \date    2022/01/7
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_TEMPLATES_H
#define ONEDRIVEAPI_TEMPLATES_H

#include "interfaces/ijsonvalue.hpp"
#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <memory>
#include <string>
#include <vector>
namespace onedriveapi {

template<typename... Args>
std::string string_format(const char* format, Args... args)
{
  std::size_t size = snprintf(nullptr, 0, format, args...);
  std::string buf;
  buf.reserve(size + 1);
  buf.resize(size);
  snprintf(&buf[0], size + 1, format, args...);
  return buf;
}

template<class T>
bool contains(std::vector<T> const& v, T const& x)
{
  return !(v.empty() || std::find(v.begin(), v.end(), x) == v.end());
}

static std::tm parseTime(std::string timeString)
{
  std::tm t = {};
  std::istringstream ss(timeString.c_str());
  ss >> std::get_time(&t, "%Y-%m-%dT%H:%M:%S");
  return t;
}

template<typename T>
std::shared_ptr<T> createFacet(IJsonValue* dataObject)
{
  std::shared_ptr<T> result = nullptr;
  if (dataObject)
  {
    result = std::make_unique<T>(dataObject);
  }
  return result;
}
} // namespace onedriveapi
#endif // TEMPLATES_H
