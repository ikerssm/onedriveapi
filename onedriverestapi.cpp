/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    onedriverestapi.cpp
/// \date    2022/01/7
/// \author  Iker Salmon San Millan
///
#include "onedriverestapi.hpp"

#include <filesystem>
#include <fstream>
#include <functional>
#include <iostream>
#include <memory.h>

#include "httpclient.hpp"
#include "templates.hpp"
#include "uploadsession.hpp"
#ifdef WITH_JSONCPP
#include "jsoncpphandler.hpp"
#endif

namespace onedriveapi {

OneDriveRestApi::OneDriveRestApi()
#ifdef WITH_JSONCPP
    : mJsonHandler(std::make_shared<JsonCppHandler>()), mHttpClient(std::make_shared<HttpClient>())
#else
    : mHttpClient(std::make_shared<HttpClient>())
#endif

{}
void OneDriveRestApi::setScope(const std::string& newScope)
{
  mScope = newScope;
}

void OneDriveRestApi::setAuthEndpoint(const std::string& newAuthEndpoint)
{
  mAuthEndpoint = newAuthEndpoint;
}

void OneDriveRestApi::setAuthRedirectUrl(const std::string& newAuthRedirectUrl)
{
  mAuthRedirectUrl = newAuthRedirectUrl;
}

void OneDriveRestApi::setTokenUrl(const std::string& newTokenUrl)
{
  mTokenUrl = newTokenUrl;
}

void OneDriveRestApi::setClientID(const std::string& newClientID)
{
  mClientID = newClientID;
}

const std::string& OneDriveRestApi::authorize(AuthorizationType type, const string& value)
{
  mResponse.clear();
  string params;
  params = "&client_id=" + mClientID;
  params += "&redirect_uri=" + mAuthRedirectUrl;
  switch (type)
  {
  case AuthorizationType::Code:
    params += "&code=" + value + "&grant_type=authorization_code";
    break;
  case AuthorizationType::RefreshToken:
    params += "&refresh_token=" + value + "&grant_type=refresh_token";
    break;
  default:
    break;
  }

  auto code = mHttpClient->httpPost(mTokenUrl, getAuthorizationHeader(), params, mResponse);
  if (code != HttpClient::ResponseCode::Ok)
  {
    std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
  }
  return mResponse;
}

const std::string OneDriveRestApi::getLoginUrl()
{
  return mAuthEndpoint + "?client_id=" + mClientID + "&scope=" + mScope +
         "&response_type=code&redirect_uri=" + mAuthRedirectUrl;
}

void OneDriveRestApi::setVerbose(bool verbose)
{
  if (mHttpClient.get())
  {
    mHttpClient->setVerbose(verbose);
  }
}

void OneDriveRestApi::setAccesToken(const std::string& newAccesToken)
{
  mAccessToken = newAccesToken;
}

const std::string& OneDriveRestApi::getAccessToken() const

{
  return mAccessToken;
}

void OneDriveRestApi::setProgressObserver(std::function<void(int)> function)
{
  if (mHttpClient.get())
  {
    mHttpClient->setProgressObserverCallback(function);
  }
}

const std::string& OneDriveRestApi::getDrive()
{
  mResponse.clear();
  string url = mGraphEndPoit;
  url += "me/drive";
  auto code = mHttpClient->httpGet(url, getAuthorizationHeader(), mResponse);
  if (code != HttpClient::Ok)
  {
    std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
  }

  return mResponse;
}

const std::string& OneDriveRestApi::getRoot()
{
  mResponse.clear();
  string url = mGraphEndPoit;
  url += "me/drive/root";
  auto code = mHttpClient->httpGet(url, getAuthorizationHeader(), mResponse);
  if (code != HttpClient::ResponseCode::Ok)
  {
    std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
  }
  return mResponse;
}

const std::string& OneDriveRestApi::getRootOfDriveID(const std::string& driveID)
{
  mResponse.clear();
  string url = string_format("%sdrives/%s/root", mGraphEndPoit.c_str(), driveID.c_str());
  auto code = mHttpClient->httpGet(url, getAuthorizationHeader(), mResponse);
  if (code != HttpClient::ResponseCode::Ok)
  {
    std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
  }
  return mResponse;
}

const std::string& OneDriveRestApi::getSharedWithMe()
{
  mResponse.clear();
  string url = mGraphEndPoit;
  url += "me/drive/sharedWithMe";
  auto code = mHttpClient->httpGet(url, getAuthorizationHeader(), mResponse);
  if (code != HttpClient::ResponseCode::Ok)
  {
    std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
  }
  return mResponse;
}

const std::string& OneDriveRestApi::listChildren(const string& driveId, const string& itemID, const string& nextLink)
{
  mResponse.clear();
  string url = mGraphEndPoit;
  if (nextLink.empty())
  {
    url += string_format("drives/%s/items/%s/children", driveId.c_str(), itemID.c_str());
    // url+="?select=id,name,eTag,cTag,deleted,file,folder,root,fileSystemInfo,remoteItem,parentReference,size";
  }
  else
  {
    url = nextLink;
  }
  auto code = mHttpClient->httpGet(url, getAuthorizationHeader(), mResponse);
  if (code != HttpClient::ResponseCode::Ok)
  {
    std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
  }
  return mResponse;
}

const std::string& OneDriveRestApi::listDriveChanges(const string& driveID, const string& nextLink)
{
  mResponse.clear();
  string url = mGraphEndPoit;
  if (nextLink.empty())
  {
    url += string_format("drives/%s/root/delta", driveID.c_str());
    // url+="?select=id,name,eTag,cTag,deleted,file,folder,root,fileSystemInfo,remoteItem,parentReference,size";
  }
  else
  {
    url = nextLink;
  }

  auto code = mHttpClient->httpGet(url, getAuthorizationHeader(), mResponse);
  if (code != HttpClient::ResponseCode::Ok)
  {
    std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
  }
  return mResponse;
}

const std::string& OneDriveRestApi::listItemChanges(const string& driveId, const string& itemID, const string& nextLink)
{
  mResponse.clear();
  string url = mGraphEndPoit;
  if (nextLink.empty())
  {
    url += string_format("drives/%s/items/%s/delta", driveId.c_str(), itemID.c_str());
    // url+="?select=id,name,eTag,cTag,deleted,file,folder,root,fileSystemInfo,remoteItem,parentReference,size";
  }
  else
  {
    url = nextLink;
  }
  auto code = mHttpClient->httpGet(url, getAuthorizationHeader(), mResponse);
  if (code != HttpClient::ResponseCode::Ok)
  {
    std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
  }
  return mResponse;
}

bool OneDriveRestApi::downloadById(const string& driveId, const string& itemID, const string& saveToPath)
{
  auto result{false};
  string url = string_format("%sdrives/%s/items/%s/content", mGraphEndPoit.c_str(), driveId.c_str(), itemID.c_str());
  auto code = mHttpClient->downloadFile(url, getAuthorizationHeader(), saveToPath);
  result = (code == HttpClient::ResponseCode::Ok);
  if (!result)
  {
    std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
  }
  return result;
}

const std::string& OneDriveRestApi::createUploadSession(const string& parentDrive,
                                                        const string& parentItem,
                                                        const string& filename,
                                                        const std::string& jsonBody)
{
  auto result = false;
  mResponse.clear();
  string url = string_format("%sdrives/%s/items/%s:/%s:/createUploadSession", mGraphEndPoit.c_str(),
                             parentDrive.c_str(), parentItem.c_str(), mHttpClient->urlEncode(filename).c_str());
  std::cout << url << std::endl;
  auto headers = getAuthorizationHeader();
  headers.push_back(pair("Accept", "application/json"));
  headers.push_back(pair("Content-Type", "application/json"));
  auto code = mHttpClient->httpPost(url, headers, jsonBody, mResponse);
  result = (code == HttpClient::Ok);
  if (!result)
  {
    std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
  }
  return mResponse;
}

const std::string& OneDriveRestApi::uploadSmallFile(const string& parentDrive,
                                                    const string& parentItem,
                                                    const string& filename,
                                                    const string& localpath,
                                                    const long fileSize)
{
  mResponse.clear();
  string url = string_format("%sdrives/%s/items/%s:/%s:/content", mGraphEndPoit.c_str(), parentDrive.c_str(),
                             parentItem.c_str(), mHttpClient->urlEncode(filename).c_str());
  auto headers = getAuthorizationHeader();
  headers.push_back(pair("Content-Type", "application/octet-stream"));
  auto code = mHttpClient->uploadFile(url, headers, localpath, fileSize, mResponse);
  if (code != HttpClient::Ok)
  {
    std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
  }
  return mResponse;
}

bool OneDriveRestApi::uploadchunk(
 const string& url, const char* data, const long initialByte, const long bytes, const long filesize)
{
  auto result = false;
  if (mHttpClient.get())
  {
    mResponse.clear();

    struct HttpClient::chunk dataChunk;
    dataChunk.chunkNumber = (int)initialByte / bytes;
    dataChunk.chunkSize = bytes;
    dataChunk.data = data;
    dataChunk.fileSize = filesize;
    list<pair<string, string>> headers;
    headers.push_back(pair("Content-Length", string_format("%d", bytes)));
    headers.push_back(
     pair("Content-Range", string_format("bytes %ld-%d/%ld", initialByte, initialByte + bytes - 1, filesize)));
    auto code = mHttpClient->uploadChunk(url, headers, &dataChunk, data, bytes, filesize, mResponse);
    std::cout << "Chunk upload result code: " << code << " For chunk number" << dataChunk.chunkNumber;
    result = (code == HttpClient::Ok || code == HttpClient::Created || code == HttpClient::Accepted);
    if (!result)
    {
      std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
    }
    if (mJsonHandler.get())
    {
      result &= mJsonHandler->parseJson(mResponse);
      std::cout << mJsonHandler->jsonAsString() << std::endl;
    }
  }

  return result;
}

const std::string&
OneDriveRestApi::deleteItem(const std::string& driveId, const std::string& itemID, const std::string& eTag)
{
  mResponse.clear();
  string url = string_format("%sdrives/%s/items/%s", mGraphEndPoit.c_str(), driveId.c_str(), itemID.c_str());
  auto headers = getAuthorizationHeader();
  if (!eTag.empty())
  {
    headers.push_back(pair("If-Match", eTag));
  }
  auto code = mHttpClient->deleteItem(url, headers, mResponse);
  if (code != HttpClient::ResponseCode::Ok)
  {
    std::cout << "Error in http petition: " << code << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)]
              << std::endl;
  }
  return mResponse;
}

const std::string& OneDriveRestApi::findAllSites(std::string& nextUrl)
{
  mResponse.clear();
  string url = mGraphEndPoit;
  url += "sites?search=*";
  auto code = mHttpClient->httpGet(url, getAuthorizationHeader(), mResponse);
  if (code != HttpClient::ResponseCode::Ok)
  {
    std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
  }
  return mResponse;
}

const std::string& OneDriveRestApi::getSiteDrives(const std::string& siteId)
{
  mResponse.clear();
  string url = string_format("%ssites/%s/drives", mGraphEndPoit.c_str(), siteId.c_str());
  auto code = mHttpClient->httpGet(url, getAuthorizationHeader(), mResponse);
  if (code != HttpClient::ResponseCode::Ok)
  {
    std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
  }
  return mResponse;
}

bool OneDriveRestApi::searchItemInSite(const std::string& siteId, const std::string& searchFor)
{
  auto result{false};
  if (mHttpClient.get())
  {
    mResponse.clear();
    string url = string_format("%ssites/%s/drive/root/search(q='{%s}')", mGraphEndPoit.c_str(), siteId.c_str(),
                               mHttpClient->urlEncode(searchFor).c_str()); // drive/root/search(q='{search-text}')
    auto code = mHttpClient->httpGet(url, getAuthorizationHeader(), mResponse);
    result = (code == HttpClient::Ok);
    if (!result)
    {
      std::cout << "Error in http petition: " << mHttpClient->ErrorStrings[HttpClient::ResponseCode(code)] << std::endl;
    }
    if (mJsonHandler.get())
    {
      result &= mJsonHandler->parseJson(mResponse);
    }
  }
  return result;
}

list<pair<string, string>> OneDriveRestApi::getAuthorizationHeader()
{
  list<pair<string, string>> headers;
  headers.push_back(pair("Authorization", "Bearer " + mAccessToken));
  return headers;
}

const std::string& OneDriveRestApi::response() const
{
  return mResponse;
}

} // namespace onedriveapi
