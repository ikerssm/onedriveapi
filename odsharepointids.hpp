/// OneDriveApi
/// Copyright (C) 2021, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    odsharepointids.hpp
/// \date    2021/11/2
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_ODSHAREPOINTIDS_HPP
#define ONEDRIVEAPI_ODSHAREPOINTIDS_HPP

#include <string>
namespace onedriveapi {
using std::string;
class ODSharepointIds
{
public:
  ODSharepointIds();

  const string& listId() const;
  void setListId(const string& newListId);
  const string& listItemId() const;
  void setListItemId(const string& newListItemId);
  const string& listIntemUniqueId() const;
  void setListIntemUniqueId(const string& newListIntemUniqueId);
  const string& siteId() const;
  void setSiteId(const string& newSiteId);
  const string& siteUrl() const;
  void setSiteUrl(const string& newSiteUrl);
  const string& tenantId() const;
  void setTenantId(const string& newTenantId);

private:
  string mListId;
  string mListItemId;
  string mListIntemUniqueId;
  string mSiteId;
  string mSiteUrl;
  string mTenantId;
  string mWebId;
};
} // namespace onedriveapi
#endif // ONEDRIVEAPI_ODSHAREPOINTIDS_HPP
