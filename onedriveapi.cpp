/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    onedriveapi.cpp
/// \date    2022/01/17
/// \author  Iker Salmon San Millan
///
#include "onedriveapi.hpp"

#ifdef WITH_JSONCPP
#include "jsoncpphandler.hpp"
#endif

#include <iostream>

namespace onedriveapi {

OneDriveApi::OneDriveApi()
#ifdef WITH_JSONCPP
    : mJsonHandler(std::make_shared<JsonCppHandler>())
#endif
{}

const std::string OneDriveApi::getLoginUrl()
{
  return mRestApi.getLoginUrl();
}

const bool OneDriveApi::authorize(OneDriveRestApi::AuthorizationType type, const std::string& value)
{
  auto result = (mJsonHandler.get() != nullptr);
  if (result)
  {
    result = mJsonHandler->parseJson(mRestApi.authorize(type, value)) && mJsonHandler->hasTokens();
    if (result)
    {
      mRestApi.setAccesToken(mJsonHandler->getKey("access_token"));
      mRefreshToken = mJsonHandler->getKey("refresh_token");
      mTokenExpiresIn = std::time(0) + mJsonHandler->getKeyAsLong("expires_in");
    }
  }
  else
  {
    std::cout << "NO JsonHandler setted. Either recompile with JSONCPP or set "
                 "manually a JsonHandler based on IJSONValue interface"
              << std::endl;
  }
  return result;
}

const bool OneDriveApi::tokenStillValid()
{
  return !mRestApi.getAccessToken().empty() && (mTokenExpiresIn - -std::time(0)) > 0;
}

void OneDriveApi::setJsonHandler(const std::shared_ptr<IJsonValue>& newJsonHandler)
{
  mJsonHandler = newJsonHandler;
}

const bool OneDriveApi::getOneDriveMainDrive(IODDrive* drive)
{
  auto result = false;
  if (drive)
  {
    renewTokenIfNeeded();
    result =
     mJsonHandler.get() && mJsonHandler->parseJson(mRestApi.getDrive()) && responseHasNoError(mJsonHandler.get());
    if (result)
    {
      result &= drive->setDriveProperties(mJsonHandler.get());
    }
  }
  return result;
}

const bool OneDriveApi::getRootOfDrive(IODDrive* drive)
{
  auto result = false;
  if (drive)
  {
    renewTokenIfNeeded();
    result = mJsonHandler.get() && mJsonHandler->parseJson(mRestApi.getRootOfDriveID(drive->id())) &&
             responseHasNoError(mJsonHandler.get());
    if (result)
    {
      result &= drive->setRoot(mJsonHandler.get());
    }
  }
  return result;
}

const bool OneDriveApi::getSharedWithMe(IODCollection* collection)
{
  auto result = false;

  if (collection)
  {
    renewTokenIfNeeded();
    if (mJsonHandler.get() && mJsonHandler->parseJson(mRestApi.getSharedWithMe()) &&
        responseHasNoError(mJsonHandler.get()))
    {
      result = collection->appendCollectionData(mJsonHandler.get());
    }
  }
  return result;
}

const bool OneDriveApi::getChildren(IODDriveItem* item)
{
  auto result = false;
  std::string nextLink;
  if (item)
  {
    renewTokenIfNeeded();
    if (mJsonHandler.get())
    {
      do
      {
        auto response = mRestApi.listChildren(item->parentReference()->driveID(), item->id(), nextLink);
        mJsonHandler->parseJson(response);
        nextLink = mJsonHandler->getKey("@odata.nextLink");
        result &= responseHasNoError(mJsonHandler.get()) && item->addChildren(mJsonHandler.get());
      } while (!nextLink.empty());
    }
  }
  return result;
}

const bool OneDriveApi::getDriveChanges(IODDrive* drive,
                                        IODCollection* changes,
                                        std::string& newDelta,
                                        const std::string& lastDelta)
{
  auto result = false;
  std::string nextLink = lastDelta;
  if (drive && changes)
  {
    renewTokenIfNeeded();
    if (mJsonHandler.get())
      do
      {
        auto response = mRestApi.listDriveChanges(drive->id(), nextLink);
        result = mJsonHandler->parseJson(mRestApi.listDriveChanges(drive->id(), nextLink));
        result &= responseHasNoError(mJsonHandler.get());
        changes->appendCollectionData(mJsonHandler.get());
        nextLink = mJsonHandler->getKey("@odata.nextLink");
      } while (!nextLink.empty());
    newDelta = mJsonHandler->getKey("@odata.deltaLink");
  }
  return result;
}

const bool OneDriveApi::getItemChanges(
 IODDrive* drive, IODDriveItem* item, IODCollection* changes, std::string& newDelta, const std::string& lastDelta)
{
  auto result = false;
  std::string nextLink = lastDelta;
  if (drive && changes)
  {
    renewTokenIfNeeded();
    if (mJsonHandler.get())
      do
      {
        result = mJsonHandler->parseJson(mRestApi.listItemChanges(drive->id(), item->id(), nextLink));
        result &= responseHasNoError(mJsonHandler.get());
        changes->appendCollectionData(mJsonHandler.get());
        nextLink = mJsonHandler->getKey("@odata.nextLink");
      } while (!nextLink.empty());
    newDelta = mJsonHandler->getKey("@odata.deltaLink");
  }
  return result;
}

bool OneDriveApi::downloadFile(IODDrive* drive, IODDriveItem* item, const std::string& saveToPath)
{
  renewTokenIfNeeded();
  return mRestApi.downloadById(drive->id(), item->id(), saveToPath);
}

bool OneDriveApi::downloadFile(const std::string& driveId, const std::string& itemID, const std::string& saveToPath)
{
  renewTokenIfNeeded();
  return mRestApi.downloadById(driveId, itemID, saveToPath);
}

bool OneDriveApi::uploadSmallFile(
 IODDrive* drive, IODDriveItem* item, const std::string& filename, const std::string& localpath, const long fileSize)
{
  auto result = false;
  if (drive && item)
  {
    result = mJsonHandler->parseJson(mRestApi.uploadSmallFile(drive->id(), item->id(), filename, localpath, fileSize));
    result &= responseHasNoError(mJsonHandler.get());
  }
  return result;
}

bool OneDriveApi::uploadSmallFile(const std::string& parentDrive,
                                  const std::string& parentItem,
                                  const std::string& filename,
                                  const std::string& localpath,
                                  const long fileSize)
{
  auto result =
   mJsonHandler->parseJson(mRestApi.uploadSmallFile(parentDrive, parentItem, filename, localpath, fileSize));
  result &= responseHasNoError(mJsonHandler.get());
  return result;
}

bool OneDriveApi::deleteItem(IODDrive* drive, IODDriveItem* item, const std::string& eTag)
{
  auto result = false;
  if (drive && item)
  {
    result = mJsonHandler->parseJson(mRestApi.deleteItem(drive->id(), item->id(), eTag));
    result &= responseHasNoError(mJsonHandler.get());
  }
  return result;
}

bool OneDriveApi::deleteItem(const std::string& driveId, const std::string& itemID, const std::string& eTag)
{
  auto result = mJsonHandler->parseJson(mRestApi.deleteItem(driveId, itemID, eTag));
  result &= responseHasNoError(mJsonHandler.get());
  return result;
}

const std::vector<string> OneDriveApi::findAllSitesID()
{
  std::vector<string> sites;
  auto nextLink = std::string();
  renewTokenIfNeeded();
  if (mJsonHandler.get())
  {
    do
    {
      if (mJsonHandler->parseJson(mRestApi.findAllSites(nextLink)) && responseHasNoError(mJsonHandler.get()))
      {
        auto sitesIds = mJsonHandler->getObjects("value");
        std::for_each(sitesIds.begin(), sitesIds.end(),
                      [&sites](const std::shared_ptr<IJsonValue>& object) { sites.push_back(object->getKey("id")); });
        nextLink = mJsonHandler->getKey("@odata.nextLink");
      }
    } while (!nextLink.empty());
  }
  return sites;
}

const std::vector<ODSite> OneDriveApi::findAllSites()
{
  std::vector<ODSite> sites;
  auto nextLink = std::string();
  renewTokenIfNeeded();
  if (mJsonHandler.get())
  {
    do
    {
      if (mJsonHandler->parseJson(mRestApi.findAllSites(nextLink)) && responseHasNoError(mJsonHandler.get()))
      {
        auto siteObjects = mJsonHandler->getObjects("value");
        std::for_each(siteObjects.begin(), siteObjects.end(),
                      [&sites](const std::shared_ptr<IJsonValue>& object) { sites.push_back(ODSite(object.get())); });
        nextLink = mJsonHandler->getKey("@odata.nextLink");
      }
    } while (!nextLink.empty());
  }
  return sites;
}

UploadSession OneDriveApi::createUploadSession(const std::string& parentDrive,
                                               const std::string& parentItem,
                                               const std::string& filename,
                                               IJsonValue* optionalBody)
{
  renewTokenIfNeeded();
  if (mJsonHandler.get())
  {
    if (mJsonHandler->parseJson(
         mRestApi.createUploadSession(parentDrive, parentItem, filename, optionalBody->jsonAsString())) &&
        responseHasNoError(mJsonHandler.get()))
    {
    }
  }
}

void OneDriveApi::renewTokenIfNeeded()
{
  if (!tokenStillValid())
  {
    mRestApi.authorize(OneDriveRestApi::AuthorizationType::RefreshToken, mRefreshToken);
  }
}

const bool OneDriveApi::responseHasNoError(IJsonValue* data)
{
  auto result = false;
  if (data)
  {
    result = data->getKey("error").empty();
  }
  return result;
}

const std::string& OneDriveApi::getRefreshToken() const
{
  return mRefreshToken;
}

void OneDriveApi::getSiteDrives(const std::string& siteId, IODCollection* drives)
{
  renewTokenIfNeeded();

  if (mJsonHandler.get())
  {
    if (mJsonHandler->parseJson(mRestApi.getSiteDrives(siteId)))
    {
      drives->appendCollectionData(mJsonHandler.get());
    }
  }
}

} // namespace onedriveapi
