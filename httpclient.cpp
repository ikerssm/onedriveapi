/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    httpclient.cpp
/// \date    2022/01/5
/// \author  Iker Salmon San Millan
///
#include "httpclient.hpp"

#include <algorithm>
#include <cstring>
#include <fstream>
#include <iostream>

#include "templates.hpp"

namespace onedriveapi {

HttpClient::HttpClient() {}

const long HttpClient::httpGet(const string& url, const list<pair<string, string>>& headers, string& response)
{
  mResponse.clear();
  CURL* curl;
  long res = 0;
  curl = curl_easy_init();
  if (curl)
  {
    struct curl_slist* headerList = nullptr;
    for (auto const& h : headers)
    {
      headerList = curl_slist_append(headerList, string_format("%s: %s", h.first.c_str(), h.second.c_str()).c_str());
    }
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerList);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &HttpClient::writeResponseCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, static_cast<void*>(this));
    curl_easy_perform(curl);
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &res);
    curl_slist_free_all(headerList);
    curl_easy_cleanup(curl);
    response = mResponse;
  }
  return res;
}

const long HttpClient::httpPost(const string& url,
                                const list<pair<string, string>>& headers,
                                const string& params,
                                string& response)
{
  mResponse.clear();
  CURL* curl;
  long res = 0;
  curl = curl_easy_init();
  if (curl)
  {
    struct curl_slist* headerList = nullptr;
    for (auto const& h : headers)
    {
      auto header = h.first;
      header += ": ";
      header += h.second;
      headerList = curl_slist_append(headerList, string_format("%s: %s", h.first.c_str(), h.second.c_str()).c_str());
    }

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerList);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &HttpClient::writeResponseCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, static_cast<void*>(this));
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, params.c_str());
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, params.size());
    curl_easy_perform(curl);
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &res);
    curl_slist_free_all(headerList);
    curl_easy_cleanup(curl);
    response = mResponse;
  }
  return res;
}

const long HttpClient::downloadFile(const string& url, const list<pair<string, string>>& headers, const string& path)
{
  CURL* curl;
  long res = 0;
  curl = curl_easy_init();
  if (curl)
  {
    struct curl_slist* headerList = nullptr;
    for (auto const& h : headers)
    {
      headerList = curl_slist_append(headerList, string_format("%s: %s", h.first.c_str(), h.second.c_str()).c_str());
    }
    FILE* fp;
    fp = fopen(path.c_str(), "wb");
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerList);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
    curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, &HttpClient::download_progress_callback);
    curl_easy_setopt(curl, CURLOPT_XFERINFODATA, static_cast<void*>(this));
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
    curl_easy_perform(curl);
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &res);
    curl_slist_free_all(headerList);
    curl_easy_cleanup(curl);
    fclose(fp);
  }
  return res;
}

const long HttpClient::uploadFile(const string& url,
                                  const list<pair<string, string>>& headers,
                                  const string& path,
                                  const long fileSize,
                                  std::string& response)
{
  mResponse.clear();
  CURL* curl;
  long res = 0;
  curl = curl_easy_init();
  if (curl)
  {
    struct curl_slist* headerList = nullptr;
    for (auto const& h : headers)
    {
      headerList = curl_slist_append(headerList, string_format("%s: %s", h.first.c_str(), h.second.c_str()).c_str());
    }
    FILE* fp;
    fp = fopen(path.c_str(), "r");
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerList);
    curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
    curl_easy_setopt(curl, CURLOPT_READDATA, fp);
    curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t)fileSize);
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &HttpClient::writeResponseCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, static_cast<void*>(this));
    curl_easy_perform(curl);
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &res);
    curl_slist_free_all(headerList);
    curl_easy_cleanup(curl);
    fclose(fp);
    response = mResponse;
  }
  return res;
}

const long HttpClient::uploadChunk(const string& url,
                                   const list<pair<string, string>>& headers,
                                   chunk* dataChunk,
                                   const char* data,
                                   const long chunkSize,
                                   const long fileSize,
                                   string& response)
{
  mResponse.clear();
  CURL* curl;
  long res = 0;
  long bytes_uploaded = 0;
  curl = curl_easy_init();
  if (curl)
  {
    struct curl_slist* headerList = nullptr;
    for (auto const& h : headers)
    {
      headerList = curl_slist_append(headerList, string_format("%s: %s", h.first.c_str(), h.second.c_str()).c_str());
    }
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerList);
    curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
    curl_easy_setopt(curl, CURLOPT_READDATA, dataChunk);
    curl_easy_setopt(curl, CURLOPT_READFUNCTION, &HttpClient::read_callback);
    curl_easy_setopt(curl, CURLOPT_INFILESIZE, (curl_off_t)chunkSize);
    curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, &HttpClient::download_progress_callback);
    curl_easy_setopt(curl, CURLOPT_XFERINFODATA, static_cast<void*>(this));
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &HttpClient::writeResponseCallback);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, static_cast<void*>(this));
    curl_easy_perform(curl);
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &res);
    curl_easy_getinfo(curl, CURLINFO_SIZE_UPLOAD, &bytes_uploaded);
    curl_slist_free_all(headerList);
    curl_easy_cleanup(curl);
    response = mResponse;
  }
  return res;
}

const long HttpClient::deleteItem(const std::string& url,
                                  const list<pair<std::string, std::string>>& headers,
                                  std::string& response)
{
  mResponse.clear();
  CURL* curl;
  long res = 0;
  curl = curl_easy_init();
  if (curl)
  {
    struct curl_slist* headerList = nullptr;
    for (auto const& h : headers)
    {
      headerList = curl_slist_append(headerList, string_format("%s: %s", h.first.c_str(), h.second.c_str()).c_str());
    }
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerList);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &HttpClient::writeResponseCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, static_cast<void*>(this));
    curl_easy_perform(curl);
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &res);
    curl_slist_free_all(headerList);
    curl_easy_cleanup(curl);
    response = mResponse;
  }
  return res;
}

const string HttpClient::urlEncode(const string& str)
{
  string result;
  CURL* curl = curl_easy_init();
  if (curl)
  {
    char* output = curl_easy_escape(curl, str.c_str(), str.size());
    if (output)
    {
      result.assign(output);
      curl_free(output);
    }
  }
  return result;
}

void HttpClient::setVerbose(const bool& vervose)
{
  mVerbose = vervose;
}

size_t HttpClient::writeResponseCallback(char* ptr, size_t size, size_t nmemb, void* userp)
{
  auto realsize = size * nmemb;
  if (userp && ptr)
  {
    static_cast<HttpClient*>(userp)->mResponse.append(ptr, ptr + realsize);
  }
  return realsize;
}

int HttpClient::download_progress_callback(
 void* clientp, curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow)
{
  if (dltotal != 0 && clientp)
  {
    auto function = static_cast<HttpClient*>(clientp)->mProgressNotifier;
    if (function)
    {
      function((float)dlnow / (float)dltotal * 100);
    }
  }
  return 0;
}

size_t HttpClient::read_callback(char* buffer, size_t size, size_t nitems, void* userdata)
{
  auto data = static_cast<HttpClient::chunk*>(userdata);
  size_t curl_size = nitems * size;
  size_t to_copy = (data->chunkSize < curl_size) ? data->chunkSize : curl_size;
  std::memcpy(buffer, data->data, to_copy);
  data->chunkSize -= to_copy;
  data->data += to_copy;
  return to_copy;
}

} // namespace onedriveapi
