/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    oditemreference.cpp
/// \date    2022/01/2
/// \author  Iker Salmon San Millan
///
#include "oditemreference.hpp"
#include "../templates.hpp"
#include "facets/sharepointids.hpp"
namespace onedriveapi {

OdItemReference::OdItemReference() {}

OdItemReference::OdItemReference(IJsonValue* data)
{
  if (data)
  {
    mDriveId = data->getKey("driveId");
    mDriveType = data->getKey("driveType");
    mId = data->getKey("id");
    mListId = data->getKey("listid");
    mName = data->getKey("name");
    mPath = data->getKey("path");
    mShareId = data->getKey("path");
    mSharepointIds = createFacet<facets::SharepointIds>(data->getObject("sharepointIds").get());
  }
}

const string& OdItemReference::driveID() const
{
  return mDriveId;
}

void OdItemReference::setDriveID(const string& newDriveID)
{
  mDriveId = newDriveID;
}

const string& OdItemReference::driveType() const
{
  return mDriveType;
}

void OdItemReference::setDriveType(const string& newDriveType)
{
  mDriveType = newDriveType;
}

const string& OdItemReference::id() const
{
  return mId;
}

void OdItemReference::setId(const string& newId)
{
  mId = newId;
}

const string& OdItemReference::listId() const
{
  return mListId;
}

void OdItemReference::setListId(const string& newListId)
{
  mListId = newListId;
}

const string& OdItemReference::name() const
{
  return mName;
}

void OdItemReference::setName(const string& newName)
{
  mName = newName;
}

const string& OdItemReference::path() const
{
  return mPath;
}

void OdItemReference::setPath(const string& newPath)
{
  mPath = newPath;
}

const string& OdItemReference::shareId() const
{
  return mShareId;
}

void OdItemReference::setShareId(const string& newShareId)
{
  mShareId = newShareId;
}

} // namespace onedriveapi
