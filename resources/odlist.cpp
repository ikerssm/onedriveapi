/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    odlist.cpp
/// \date    2022/01/13
/// \author  Iker Salmon San Millan
///
#include "odlist.hpp"
namespace onedriveapi {

ODList::ODList() {}

ODList::ODList(IJsonValue* data)
{
  if (data)
  {
    mDisplayName = data->getKey("displayName");
    mlist = std::make_unique<ListInfo>(data->getObject("list").get());
    mSystem = data->getObject("system").get();
    setBaseProperties(data);
  }
}
} // namespace onedriveapi
