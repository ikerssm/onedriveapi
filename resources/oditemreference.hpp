/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    oditemreference.hpp
/// \date    2022/01/2
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_ODITEMREFERENCE_HPP
#define ONEDRIVEAPI_ODITEMREFERENCE_HPP
#include <string>

#include "../facets/sharepointids.hpp"
#include "../interfaces/ijsonvalue.hpp"
#include "../onedriveenums.hpp"

namespace onedriveapi {
using std::string;

class OdItemReference
{
public:
  OdItemReference();
  explicit OdItemReference(IJsonValue* data);

  const string& driveID() const;
  void setDriveID(const string& newDriveID);
  const string& driveType() const;
  void setDriveType(const std::string& newDriveType);
  const string& id() const;
  void setId(const string& newId);
  const string& listId() const;
  void setListId(const string& newListId);
  const string& name() const;
  void setName(const string& newName);
  const string& path() const;
  void setPath(const string& newPath);
  const string& shareId() const;
  void setShareId(const string& newShareId);
  void printData()
  {
    std::cout << "Item reference: " << std::endl;
    std::cout << "                 Drive ID: " << mDriveId << std::endl;
    std::cout << "                 Drive Type: " << mDriveType << std::endl;
    std::cout << "                 id: " << mId << std::endl;
    std::cout << "                 List id: " << mListId << std::endl;
    std::cout << "                 name: " << mName << std::endl;
    std::cout << "                 Path: " << mPath << std::endl;
    std::cout << "                 Share Id: " << mShareId << std::endl;

    if (mSharepointIds)
    {
      mSharepointIds->printData();
    }
  }

private:
  string mDriveId;
  string mDriveType;
  string mId;
  string mListId;
  string mName;
  string mPath;
  string mShareId;
  std::shared_ptr<facets::SharepointIds> mSharepointIds = nullptr;
};
} // namespace onedriveapi

#endif // ONEDRIVEAPI_ODITEMREFERENCE_HPP
