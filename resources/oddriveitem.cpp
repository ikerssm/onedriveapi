/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    oddriveitem.cpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
#include "oddriveitem.hpp"

#include <iostream>

namespace onedriveapi {

ODDriveItem::ODDriveItem() {}

bool ODDriveItem::setDriveItemProperties(IJsonValue* data)
{
  auto result = false;
  {
    if (data)
    {
      mAudio = createFacet<facets::Audio>(data->getObject("audio").get());
      mCTag = data->getKey("cTag");
      mDeleted = createFacet<facets::Deleted>(data->getObject("deleted").get());
      mFile = createFacet<facets::File>(data->getObject("file").get());
      mFileSystemInfo = createFacet<facets::FileSystemInfo>(data->getObject("fileSystemInfo").get());
      mFolder = createFacet<facets::Folder>(data->getObject("folder").get());
      mImage = createFacet<facets::Image>(data->getObject("image").get());
      mLocation = createFacet<facets::Location>(data->getObject("location").get());
      mIsMalware = data->getObject("malware").get();
      mPackage = createFacet<facets::Package>(data->getObject("package").get());
      mPhoto = createFacet<facets::Photo>(data->getObject("photo").get());
      mPublication = createFacet<facets::Publication>(data->getObject("publication").get());
      mRemoteItem = createFacet<facets::RemoteItem>(data->getObject("remoteItem").get());
      mIsRoot = data->getObject("root").get();
      mSearchResult = createFacet<SearchResult>(data->getObject("searchResult").get());
      mShared = createFacet<facets::Shared>(data->getObject("shared").get());
      mSharepointIds = createFacet<facets::SharepointIds>(data->getObject("sharepointIds").get());
      mSize = data->getKeyAsLong("size");
      mSpecialFolder = createFacet<facets::SpecialFolder>(data->getObject("specialFolder").get());
      mVideo = createFacet<facets::Video>(data->getObject("video").get());
      mWebDavUrl = data->getKey("webDavUrl");
      result = setBaseProperties(data);
    }
  }
  return result;
}

void ODDriveItem::printItem()
{
  printBaseItemData();
  if (mAudio)
  {
    mAudio->printData();
  }
  std::cout << "ctag: " << mCTag << std::endl;
  if (mDeleted)
  {
    std::cout << "Deleted: " << mDeleted->state << std::endl;
  }
  if (mFileSystemInfo)
  {
    mFileSystemInfo->printData();
  }
  if (mFolder)
  {
    mFolder->printData();
  }
  if (mImage)
  {
    mImage->printData();
  }
  if (mIsMalware)
  {
    std::cout << "Is malware: true " << std::endl;
  }
  if (mPackage)
  {
    std::cout << "Package: " << mPackage->type << std::endl;
  }
  if (mPhoto)
  {
    mPhoto->printData();
  }
  if (mPublication)
  {
    mPublication->printData();
  }
  if (mRemoteItem)
  {
    mRemoteItem->printData();
  }
  if (mIsRoot)
  {
    std::cout << "Is root: true" << std::endl;
  }
  if (mShared)
  {
    mShared->printData();
  }
  if (mSharepointIds)
  {
    mSharepointIds->printData();
  }
  std::cout << "Size: " << mSize << std::endl;
  if (mSpecialFolder)
  {
    std::cout << "Special Folder: " << mSpecialFolder->name << std::endl;
  }
  if (mVideo)
  {
    mVideo->printData();
  }
  std::cout << "webDavUrl: " << mWebDavUrl << std::endl;
}

const std::shared_ptr<facets::Audio>& ODDriveItem::audio() const
{
  return mAudio;
}

const std::string& ODDriveItem::cTag() const
{
  return mCTag;
}

const std::shared_ptr<facets::Deleted>& ODDriveItem::deleted() const
{
  return mDeleted;
}

const std::shared_ptr<facets::File>& ODDriveItem::file() const
{
  return mFile;
}

const std::shared_ptr<facets::FileSystemInfo>& ODDriveItem::fileSystemInfo() const
{
  return mFileSystemInfo;
}

const std::shared_ptr<facets::Folder>& ODDriveItem::folder() const
{
  return mFolder;
}

const std::shared_ptr<facets::Image>& ODDriveItem::image() const
{
  return mImage;
}

const std::shared_ptr<facets::Location>& ODDriveItem::location() const
{
  return mLocation;
}

bool ODDriveItem::isMalware() const
{
  return mIsMalware;
}

const std::shared_ptr<facets::Package>& ODDriveItem::package() const
{
  return mPackage;
}

const std::shared_ptr<facets::Photo>& ODDriveItem::photo() const
{
  return mPhoto;
}

const std::shared_ptr<facets::Publication>& ODDriveItem::publication() const
{
  return mPublication;
}

const std::shared_ptr<facets::RemoteItem>& ODDriveItem::remoteItem() const
{
  return mRemoteItem;
}

bool ODDriveItem::isRoot() const
{
  return mIsRoot;
}

const std::shared_ptr<SearchResult>& ODDriveItem::searchResult() const
{
  return mSearchResult;
}

const std::shared_ptr<facets::Shared>& ODDriveItem::shared() const
{
  return mShared;
}

const std::shared_ptr<facets::SharepointIds>& ODDriveItem::sharepointIds() const
{
  return mSharepointIds;
}

unsigned long ODDriveItem::size() const
{
  return mSize;
}

const std::shared_ptr<facets::SpecialFolder>& ODDriveItem::specialFolder() const
{
  return mSpecialFolder;
}

const std::shared_ptr<facets::Video>& ODDriveItem::video() const
{
  return mVideo;
}

const std::string& ODDriveItem::webDavUrl() const
{
  return mWebDavUrl;
}

const std::vector<std::shared_ptr<ODDriveItem>>& ODDriveItem::children() const
{
  return mChildren;
}

bool ODDriveItem::addChildren(IJsonValue* data)
{
  auto previousChildern = mChildren.size();
  if (data)
  {
    auto objects = data->getObjects("value");
    for (const auto& object : objects)
    {
      auto item = std::make_shared<ODDriveItem>();
      item->setDriveItemProperties(object.get());
      mChildren.push_back(item);
    }
  }
  return previousChildern < mChildren.size();
}
} // namespace onedriveapi
