/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    oddriveitemcollection.cpp
/// \date    2022/01/15
/// \author  Iker Salmon San Millan
///
#include "oddriveitemcollection.hpp"
namespace onedriveapi {

ODDriveItemCollection::ODDriveItemCollection() {}

const bool ODDriveItemCollection::appendCollectionData(IJsonValue* data)
{
  if (data)
  {
    auto objects = data->getObjects("value");
    for (const auto& object : objects)
    {
      ODDriveItem item;
      item.setDriveItemProperties(object.get());
      mCollection.push_back(item);
    }
  }
  return mCollection.size() > 0;
}
} // namespace onedriveapi
