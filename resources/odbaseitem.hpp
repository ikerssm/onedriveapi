/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    odbaseitem.hpp
/// \date    2022/01/2
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_ODBASEITEM_HPP
#define ONEDRIVEAPI_ODBASEITEM_HPP

#include <ctime>
#include <memory>
#include <string>
#include <vector>

#include "../interfaces/ijsonvalue.hpp"
#include "../interfaces/iodbaseitem.hpp"
#include "odidentity.hpp"
#include "oditemreference.hpp"

namespace onedriveapi {

class ODBaseItem : public IODBaseItem
{
public:
  virtual bool setBaseProperties(IJsonValue* data) override;

  virtual const std::string& id() const override;
  const std::string& name() const;
  const std::string& webUrl() const;
  const std::vector<ODIdentity>& createdBy() const;
  const std::tm& createdDateTime() const;
  const std::string& description() const;
  const std::string& etag() const;
  const std::vector<ODIdentity>& lastModifiedBy() const;
  const std::tm& lastModifiedDateTime() const;
  void printBaseItemData();

protected:
  std::string mId;
  std::string mName;
  std::string mWebUrl;
  std::vector<ODIdentity> mCreatedBy;
  std::tm mCreatedDateTime;
  std::string mDescription;
  std::string mEtag;
  std::vector<ODIdentity> mLastModifiedBy;
  std::tm mLastModifiedDateTime;
};

} // namespace onedriveapi
#endif // ONEDRIVEAPI_ODBASEITEM_HPP
