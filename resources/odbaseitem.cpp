/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    odbaseitem.cpp
/// \date    2022/01/2
/// \author  Iker Salmon San Millan
///
#include "odbaseitem.hpp"

#include <iostream>

#include "templates.hpp"

namespace onedriveapi {

bool ODBaseItem::setBaseProperties(IJsonValue* data)
{
  if (data)
  {
    mCreatedDateTime = parseTime(data->getKey("createdDateTime"));
    mDescription = data->getKey("description");
    mId = data->getKey("id");
    mEtag = data->getKey("eTag");
    mLastModifiedDateTime = parseTime(data->getKey("lastModifiedDateTime"));
    mName = data->getKey("name");
    mWebUrl = data->getKey("webUrl");
    mCreatedBy = createIdentitySet(data->getObject("createdBy").get());
    mLastModifiedBy = createIdentitySet(data->getObject("lastModifiedBy").get());
    mParentReference = createFacet<OdItemReference>(data->getObject("parentReference").get());
  }
  return !mId.empty();
}

const std::string& ODBaseItem::id() const
{
  return mId;
}

const std::string& ODBaseItem::name() const
{
  return mName;
}

const std::string& ODBaseItem::webUrl() const
{
  return mWebUrl;
}

const std::vector<ODIdentity>& ODBaseItem::createdBy() const
{
  return mCreatedBy;
}

const std::tm& ODBaseItem::createdDateTime() const
{
  return mCreatedDateTime;
}

const std::string& ODBaseItem::description() const
{
  return mDescription;
}

const std::string& ODBaseItem::etag() const
{
  return mEtag;
}

const std::vector<ODIdentity>& ODBaseItem::lastModifiedBy() const
{
  return mLastModifiedBy;
}

const std::tm& ODBaseItem::lastModifiedDateTime() const
{
  return mLastModifiedDateTime;
}

void ODBaseItem::printBaseItemData()
{
  std::cout << "id: " << mId << std::endl;
  std::cout << "name: " << mName << std::endl;
  std::cout << "webUrl: " << mWebUrl << std::endl;
  std::cout << "created by: ";
  for (const auto& identity : mCreatedBy)
  {
    identity.print();
  }
  char date[80];
  strftime(date, 80, "%Ec", &mCreatedDateTime);
  std::cout << "created at: " << date << std::endl;
  std::cout << "description: " << mDescription << std::endl;
  std::cout << "eTag: " << mEtag << std::endl;
  std::cout << "last modify by: ";
  for (const auto& identity : mLastModifiedBy)
  {
    identity.print();
  }
  strftime(date, 80, "%Ec", &mLastModifiedDateTime);
  std::cout << "modified at: " << date << std::endl;
  if (mParentReference)
  {
    mParentReference->printData();
  }
}
} // namespace onedriveapi
