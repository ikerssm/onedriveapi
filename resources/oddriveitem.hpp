/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    oddriveitem.hpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
/// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/resources/driveitem?view=odsp-graph-online
#ifndef ONEDRIVEAPI_ODDRIVEITEM_HPP
#define ONEDRIVEAPI_ODDRIVEITEM_HPP
#include <memory>
#include <vector>

#include "../facets/audio.hpp"
#include "../facets/deleted.hpp"
#include "../facets/file.hpp"
#include "../facets/filesysteminfo.hpp"
#include "../facets/folder.hpp"
#include "../facets/image.hpp"
#include "../facets/location.hpp"
#include "../facets/odquota.hpp"
#include "../facets/package.hpp"
#include "../facets/photo.hpp"
#include "../facets/publication.hpp"
#include "../facets/remoteitem.hpp"
#include "../facets/searchresult.hpp"
#include "../facets/sharepointids.hpp"
#include "../facets/video.hpp"
#include "../interfaces/ijsonvalue.hpp"
#include "../interfaces/ioddriveitem.hpp"
#include "../onedriveapi_global.hpp"
#include "odbaseitem.hpp"

namespace onedriveapi {

class ONEDRIVEAPI_EXPORT ODDriveItem : public IODDriveItem, public ODBaseItem
{
  using DriveItemPtr = std::shared_ptr<ODDriveItem>;

public:
  ODDriveItem();
  virtual bool setDriveItemProperties(IJsonValue* data) override;
  virtual void printItem() override;
  virtual bool addChildren(IJsonValue* data) override;

  const std::shared_ptr<facets::Audio>& audio() const;
  const std::string& cTag() const;
  const std::shared_ptr<facets::Deleted>& deleted() const;
  const std::shared_ptr<facets::File>& file() const;
  const std::shared_ptr<facets::FileSystemInfo>& fileSystemInfo() const;
  const std::shared_ptr<facets::Folder>& folder() const;
  const std::shared_ptr<facets::Image>& image() const;
  const std::shared_ptr<facets::Location>& location() const;
  bool isMalware() const;
  const std::shared_ptr<facets::Package>& package() const;
  const std::shared_ptr<facets::Photo>& photo() const;
  const std::shared_ptr<facets::Publication>& publication() const;
  const std::shared_ptr<facets::RemoteItem>& remoteItem() const;
  bool isRoot() const;
  const std::shared_ptr<SearchResult>& searchResult() const;
  const std::shared_ptr<facets::Shared>& shared() const;
  const std::shared_ptr<facets::SharepointIds>& sharepointIds() const;
  unsigned long size() const;
  const std::shared_ptr<facets::SpecialFolder>& specialFolder() const;
  const std::shared_ptr<facets::Video>& video() const;
  const std::string& webDavUrl() const;
  const std::vector<std::shared_ptr<ODDriveItem>>& children() const;

protected:
  // properties
  std::shared_ptr<facets::Audio> mAudio = nullptr;
  std::string mCTag;
  std::shared_ptr<facets::Deleted> mDeleted = nullptr;
  std::shared_ptr<facets::File> mFile = nullptr;
  std::shared_ptr<facets::FileSystemInfo> mFileSystemInfo = nullptr;
  std::shared_ptr<facets::Folder> mFolder = nullptr;
  std::shared_ptr<facets::Image> mImage = nullptr;
  std::shared_ptr<facets::Location> mLocation = nullptr;
  bool mIsMalware = false;
  std::shared_ptr<facets::Package> mPackage = nullptr;
  std::shared_ptr<facets::Photo> mPhoto = nullptr;
  std::shared_ptr<facets::Publication> mPublication = nullptr;
  std::shared_ptr<facets::RemoteItem> mRemoteItem = nullptr;
  bool mIsRoot = false;
  std::shared_ptr<SearchResult> mSearchResult = nullptr;
  std::shared_ptr<facets::Shared> mShared = nullptr;
  std::shared_ptr<facets::SharepointIds> mSharepointIds = nullptr;
  unsigned long mSize = 0;
  std::shared_ptr<facets::SpecialFolder> mSpecialFolder = nullptr;
  std::shared_ptr<facets::Video> mVideo = nullptr;
  std::string mWebDavUrl;
  // relationshipts
  std::vector<std::shared_ptr<ODDriveItem>> mChildren;

  // IODBaseItem interface
public:
  virtual bool setBaseProperties(IJsonValue* data) override { return ODBaseItem::setBaseProperties(data); }
  virtual const string& id() const override { return ODBaseItem::id(); }
};
} // namespace onedriveapi
#endif // ONEDRIVEAPI_ODDRIVEITEM_HPP
