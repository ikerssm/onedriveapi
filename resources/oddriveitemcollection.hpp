/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    oddriveitemcollection.hpp
/// \date    2022/01/15
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_ODDRIVEITEMCOLLECTION_HPP
#define ONEDRIVEAPI_ODDRIVEITEMCOLLECTION_HPP
#include "../interfaces/ijsonvalue.hpp"
#include "../interfaces/iodcollection.hpp"
#include "../onedriveapi_global.hpp"
#include "oddriveitem.hpp"

namespace onedriveapi {

class ONEDRIVEAPI_EXPORT ODDriveItemCollection : public IODCollection
{
public:
  ODDriveItemCollection();

  // IODCollection interface
  const bool appendCollectionData(IJsonValue* data) override;

protected:
  std::vector<ODDriveItem> mCollection;
};
} // namespace onedriveapi
#endif // ONEDRIVEAPI_ODDRIVEITEMCOLLECTION_HPP
