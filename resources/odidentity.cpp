/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    odidentity.cpp
/// \date    2022/01/2
/// \author  Iker Salmon San Millan
///
#include "odidentity.hpp"

#include <iostream>

namespace onedriveapi {

ODIdentity::ODIdentity() {}

ODIdentity::ODIdentity(const std::string& type, IJsonValue* data) : mType(type)
{
  if (data)
  {
    mDisplayName = data->getKey("displayName");
    mId = data->getKey("id");
    mEmail = data->getKey("email");
  }
}

const string& ODIdentity::id() const
{
  return mId;
}

void ODIdentity::setId(const string& newId)
{
  mId = newId;
}

const string& ODIdentity::displayName() const
{
  return mDisplayName;
}

void ODIdentity::setDisplayName(const string& newDisplayName)
{
  mDisplayName = newDisplayName;
}

const string& ODIdentity::type() const
{
  return mType;
}

void ODIdentity::setType(const string& newType)
{
  mType = newType;
}

const string& ODIdentity::email() const
{
  return mEmail;
}

void ODIdentity::setEmail(const string& newEmail)
{
  mEmail = newEmail;
}

const void ODIdentity::print() const
{
  std::cout << mType << std::endl;
  std::cout << "              Display name: " << mDisplayName << std::endl;
  std::cout << "              Email: " << mEmail << std::endl;
  std::cout << "              ID: " << mId << std::endl;
}

} // namespace onedriveapi
