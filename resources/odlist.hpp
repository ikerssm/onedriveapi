/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    odlist.hpp
/// \date    2022/01/13
/// \author  Iker Salmon San Millan
///
/// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/resources/list?view=odsp-graph-online
#ifndef ONEDRIVEAPI_ODLIST_HPP
#define ONEDRIVEAPI_ODLIST_HPP

#include "../interfaces/ijsonvalue.hpp"
#include "odbaseitem.hpp"
#include "oditemactivity.hpp"

namespace onedriveapi {
struct ListInfo
{
  ListInfo() {}
  ListInfo(IJsonValue* data)
  {
    if (data)
    {
      contentTypesEnabled = data->getKeyAsBool("contentTypesEnabled");
      hidden = data->getKeyAsBool("hidden");
      templateType = data->getKey("template");
    }
  }
  bool contentTypesEnabled;
  bool hidden;
  std::string templateType;
};
class ODDrive;
class ODList : public ODBaseItem
{
public:
  ODList();
  ODList(IJsonValue* data);

protected:
  // properties
  std::string mDisplayName;
  std::shared_ptr<ListInfo> mlist;
  bool mSystem; // sysem managed list

  // relationships
  // TODO
};
} // namespace onedriveapi
#endif // ONEDRIVEAPI_ODLIST_HPP
