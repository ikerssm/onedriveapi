/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    odsite.cpp
/// \date    2022/01/30
/// \author  Iker Salmon San Millan
///
#include "odsite.hpp"

#include <iostream>

#include "templates.hpp"

namespace onedriveapi {

ODSite::ODSite() {}

ODSite::ODSite(IJsonValue* data)
{
  if (data)
  {
    mCreatedDateTime = parseTime(data->getKey("createdDateTime"));
    mDescription = data->getKey("description");
    mId = data->getKey("id");
    mLastModifiedDateTime = parseTime(data->getKey("lastModifiedDateTime"));
    mName = data->getKey("name");
    mWebUrl = data->getKey("webUrl");
  }
}

const std::string& ODSite::id() const
{
  return mId;
}

const std::string& ODSite::name() const
{
  return mName;
}

const std::string& ODSite::webUrl() const
{
  return mWebUrl;
}

const std::tm& ODSite::createdDateTime() const
{
  return mCreatedDateTime;
}

const std::string& ODSite::description() const
{
  return mDescription;
}

const std::tm& ODSite::lastModifiedDateTime() const
{
  return mLastModifiedDateTime;
}

void ODSite::printData()
{
  std::cout << "id: " << mId << std::endl;
  std::cout << "name: " << mName << std::endl;
  std::cout << "webUrl: " << mWebUrl << std::endl;
  std::cout << "created by: ";
  char date[80];
  strftime(date, 80, "%Ec", &mCreatedDateTime);
  std::cout << "created at: " << date << std::endl;
  std::cout << "description: " << mDescription << std::endl;
  strftime(date, 80, "%Ec", &mLastModifiedDateTime);
  std::cout << "modified at: " << date << std::endl;
}

std::vector<ODDrive*> ODSite::drives() const
{
  return mDrives;
}

void ODSite::setDrives(const std::vector<ODDrive*>& drives)
{
  mDrives = drives;
}

} // namespace onedriveapi
