/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    oddrivecollection.hpp
/// \date    2022/5/26
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_ODDRIVECOLLECTION_HPP
#define ONEDRIVEAPI_ODDRIVECOLLECTION_HPP
#include "../interfaces/iodcollection.hpp"
#include "../onedriveapi_global.hpp"
#include "oddrive.hpp"

namespace onedriveapi {

class ONEDRIVEAPI_EXPORT ODDriveCollection : public IODCollection
{
public:
  ODDriveCollection();

  // IODCollection interface
public:
  const virtual bool appendCollectionData(IJsonValue* data) override;

  const std::vector<ODDrive>& collection() const;

protected:
  std::vector<ODDrive> mCollection;
};

} // namespace onedriveapi
#endif // ONEDRIVEAPI_ODDRIVECOLLECTION_HPP
