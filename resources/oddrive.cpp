/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    oddrive.cpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
#include "oddrive.hpp"
namespace onedriveapi {

ODDrive::ODDrive() {}

const bool ODDrive::setDriveProperties(IJsonValue* data)
{
  auto result = false;
  if (data)
  {
    mDriveType = data->getKey("driveType");
    mOwner = createIdentitySet(data->getObject("owner").get());
    mQuota = createFacet<facets::ODQuota>(data->getObject("quota").get());
    mSharepointIds = createFacet<facets::SharepointIds>(data->getObject("sharepointIds").get());
    result = ODBaseItem::setBaseProperties(data);
  }
  return result && !mDriveType.empty();
}

const bool ODDrive::setRoot(IJsonValue* data)
{
  auto result = false;
  if (data)
  {
    mRoot = std::make_shared<ODDriveItem>();
    result = mRoot->setDriveItemProperties(data);
    if (!result)
    {
      mRoot = nullptr;
    }
  }
  return result;
}

const void ODDrive::printData()
{
  printBaseItemData();
  std::cout << "Drive type: " << mDriveType << std::endl;
  std::cout << "Owner:" << std::endl;
  for (const auto& id : mOwner)
  {
    id.print();
  }
  if (mQuota)
  {
    mQuota->printData();
  }
  if (mSharepointIds)
  {
    mSharepointIds->printData();
  }
  if (mRoot)
  {
    mRoot->printItem();
  }
}

const string& ODDrive::driveType() const
{
  return mDriveType;
}

const std::vector<ODIdentity>& ODDrive::owner() const
{
  return mOwner;
}

const std::shared_ptr<facets::ODQuota>& ODDrive::quota() const
{
  return mQuota;
}

const std::shared_ptr<facets::SharepointIds>& ODDrive::sharepointIds() const
{
  return mSharepointIds;
}

const std::shared_ptr<ODDriveItem>& ODDrive::root() const
{
  return mRoot;
}

} // namespace onedriveapi
