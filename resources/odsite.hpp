/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    odsite.hpp
/// \date    2022/01/30
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_ODSITE_HPP
#define ONEDRIVEAPI_ODSITE_HPP
#include <ctime>
#include <string>
#include <vector>

#include "../interfaces/ijsonvalue.hpp"
#include "../onedriverestapi.hpp"
#include "oddrive.hpp"

namespace onedriveapi {

class ONEDRIVEAPI_EXPORT ODSite
{
public:
  ODSite();
  ODSite(IJsonValue* data);

  const std::string& id() const;
  const std::string& name() const;
  const std::string& webUrl() const;
  const std::tm& createdDateTime() const;
  const std::string& description() const;
  const std::tm& lastModifiedDateTime() const;
  void printData();

  std::vector<ODDrive*> drives() const;
  void setDrives(const std::vector<ODDrive*>& drives);

protected:
  std::string mId;
  std::string mName;
  std::string mWebUrl;
  std::tm mCreatedDateTime;
  std::string mDescription;
  std::tm mLastModifiedDateTime;
  std::vector<ODDrive*> mDrives;
};
} // namespace onedriveapi

#endif // ONEDRIVEAPI_ODSITE_HPP
