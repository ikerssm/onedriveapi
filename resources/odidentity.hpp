/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    odidentity.hpp
/// \date    2022/01/2
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_ODIDENTITY_HPP
#define ONEDRIVEAPI_ODIDENTITY_HPP
#include <string>
#include <vector>

#include "../onedriveapi_global.hpp"
#include "../interfaces/ijsonvalue.hpp"
#include "../templates.hpp"

namespace onedriveapi {
using std::string;
class ONEDRIVEAPI_EXPORT ODIdentity
{
public:
  ODIdentity();
  ODIdentity(const std::string& type, IJsonValue* data);

  const string& id() const;
  void setId(const string& newId);

  const string& displayName() const;
  void setDisplayName(const string& newDisplayName);

  const string& type() const;
  void setType(const string& newType);

  const string& email() const;
  void setEmail(const string& newEmail);

  const void print() const;

private:
  string mId;
  string mDisplayName;
  string mType;
  string mEmail;
};

static const std::vector<string> KODIdentityTypes = {"user", "group", "device", "application"};

static std::vector<ODIdentity> createIdentitySet(IJsonValue* data)
{
  std::vector<ODIdentity> result;
  if (data)
  {
    for (const auto& key : data->getKeys())
    {
      if (contains(KODIdentityTypes, key))
      {
        result.push_back(ODIdentity(key, data->getObject(key).get()));
      }
    }
  }
  return result;
}
} // namespace onedriveapi
#endif // ONEDRIVEAPI_ODIDENTITY_HPP
