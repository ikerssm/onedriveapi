/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    oditemactivity.hpp
/// \date    2022/01/4
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_ODITEMACTIVITY_HPP
#define ONEDRIVEAPI_ODITEMACTIVITY_HPP
#include <ctime>
#include <string>

#include "odidentity.hpp"
#include "oditemaction.hpp"

namespace onedriveapi {

using std::string;

class ODItemActivity
{
public:
  ODItemActivity();

private:
  string mId;
  ODItemAction mAction;
  ODIdentity mActor;
  std::time_t mObserved;
  std::time_t mRecorded;
};
} // namespace onedriveapi

#endif // ONEDRIVEAPI_ODITEMACTIVITY_HPP
