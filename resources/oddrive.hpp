/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    oddrive.hpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
/// https://docs.microsoft.com/en-us/onedrive/developer/rest-api/resources/drive?view=odsp-graph-online
#ifndef ONEDRIVEAPI_ODDRIVE_HPP
#define ONEDRIVEAPI_ODDRIVE_HPP
#include <memory>

#include "../facets/odquota.hpp"
#include "../facets/sharepointids.hpp"
#include "../interfaces/ijsonvalue.hpp"
#include "../interfaces/ioddrive.hpp"
#include "../onedriveapi_global.hpp"
#include "odbaseitem.hpp"
#include "oddriveitem.hpp"
#include "oditemactivity.hpp"
#include "odlist.hpp"

namespace onedriveapi {

class ONEDRIVEAPI_EXPORT ODDrive : public IODDrive, public ODBaseItem
{
public:
  ODDrive();
  virtual const bool setDriveProperties(IJsonValue* data) override;
  virtual const bool setRoot(IJsonValue* data) override;
  virtual const void printData() override;

  // IODBaseItem interface
  virtual bool setBaseProperties(IJsonValue* data) override { return ODBaseItem::setBaseProperties(data); }
  virtual const string& id() const override { return ODBaseItem::id(); }

  const std::string& driveType() const;
  const std::vector<ODIdentity>& owner() const;
  const std::shared_ptr<facets::ODQuota>& quota() const;
  const std::shared_ptr<facets::SharepointIds>& sharepointIds() const;

  const std::shared_ptr<ODDriveItem>& root() const;

protected:
  // properties
  std::string mDriveType;
  std::vector<ODIdentity> mOwner;
  std::shared_ptr<facets::ODQuota> mQuota;
  std::shared_ptr<facets::SharepointIds> mSharepointIds = nullptr;
  // relationships:
  std::shared_ptr<ODDriveItem> mRoot = nullptr;
  // TODO:
  // std::vector<DriveItemPtr> mItems;
  // std::vector<ODItemActivity> mActivities;
  // std::vector<DriveItemPtr> mSpecial;
  // std::shared_ptr<ODList> mList;
  //  using DriveItemPtr = std::shared_ptr<IODDriveItem>;

  // public:
  //  ODDrive();
  //  virtual const bool setDriveProperties(IJsonValue* data) override;
  //  //  virtual const bool setRoot(std::shared_ptr<IODDriveItem> root, IJsonValue* data) override;
  //  virtual void printData() override;
};
} // namespace onedriveapi
#endif // ONEDRIVEAPI_ODDRIVE_HPP
