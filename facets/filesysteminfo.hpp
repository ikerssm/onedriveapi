/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    filesysteminfo.hpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_FILESYSTEMINFO_HPP
#define ONEDRIVEAPI_FILESYSTEMINFO_HPP

#include <ctime>
#include <iostream>

#include "../interfaces/ijsonvalue.hpp"
#include "../templates.hpp"

namespace onedriveapi {
namespace facets {
struct FileSystemInfo
{
  FileSystemInfo() {}
  FileSystemInfo(IJsonValue* jsonObject)
  {
    if (jsonObject)
    {
      createdDateTime = parseTime(jsonObject->getKey("createdDateTime"));
      lastAccessedDateTime = parseTime(jsonObject->getKey("lastAccessedDateTime"));
      lastModifiedDateTime = parseTime(jsonObject->getKey("lastModifiedDateTime"));
    }
  }
  void printData()
  {
    char date[80];
    std::cout << "FilesystemInfo: " << std::endl;
    strftime(date, 80, "%Ec", &createdDateTime);
    std::cout << "               createdDateTime: " << date << std::endl;
    strftime(date, 80, "%Ec", &lastAccessedDateTime);
    std::cout << "               lastAccessedDateTime: " << date << std::endl;
    strftime(date, 80, "%Ec", &lastModifiedDateTime);
    std::cout << "               lastModifiedDateTime: " << date << std::endl;
  }
  std::tm createdDateTime;
  std::tm lastAccessedDateTime;
  std::tm lastModifiedDateTime;
};

} // namespace facets
} // namespace onedriveapi
#endif // ONEDRIVEAPI_FILESYSTEMINFO_HPP
