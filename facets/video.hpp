/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    video.hpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_VIDEO_HPP
#define ONEDRIVEAPI_VIDEO_HPP

#include <iostream>
#include <string>

#include "../interfaces/ijsonvalue.hpp"

namespace onedriveapi {
namespace facets {

struct Video
{
  Video() {}
  Video(IJsonValue* jsonObject)
  {
    if (jsonObject)
    {
      audioBitsPerSample = jsonObject->getKeyAsLong("audioBitsPerSample");
      audioChannels = jsonObject->getKeyAsLong("audioChannels");
      audioFormat = jsonObject->getKey("audioFormat");
      audioSamplesPerSecond = jsonObject->getKeyAsLong("audioSamplesPerSecond");
      bitrate = jsonObject->getKeyAsLong("bitrate");
      duration = jsonObject->getKeyAsLong("duration");
      fourCC = jsonObject->getKey("fourCC");
      frameRate = jsonObject->getKeyAsDouble("frameRate");
      height = jsonObject->getKeyAsLong("height");
      width = jsonObject->getKeyAsLong("width");
    }
  }
  void printData()
  {
    std::cout << "Video:" << std::endl;
    std::cout << "      audioBitsPerSample: " << audioBitsPerSample << std::endl;
    std::cout << "      audioChannels: " << audioChannels << std::endl;
    std::cout << "      audioFormat: " << audioFormat << std::endl;
    std::cout << "      audioSamplesPerSecond: " << audioSamplesPerSecond << std::endl;
    std::cout << "      bitrate: " << bitrate << std::endl;
    std::cout << "      duration: " << duration << std::endl;
    std::cout << "      fourCC: " << fourCC << std::endl;
    std::cout << "      frameRate: " << frameRate << std::endl;
    std::cout << "      width:" << width << std::endl;
    std::cout << "      height:" << height << std::endl;
  }
  int audioBitsPerSample;
  int audioChannels;
  std::string audioFormat;
  int audioSamplesPerSecond;
  int bitrate;
  unsigned long duration;
  std::string fourCC;
  double frameRate;
  int height;
  int width;
};

} // namespace facets
} // namespace onedriveapi
#endif // ONEDRIVEAPI_VIDEO_HPP
