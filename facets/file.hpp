/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    file.hpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_FILE_HPP
#define ONEDRIVEAPI_FILE_HPP

#include <iostream>
#include <string>

#include "../interfaces/ijsonvalue.hpp"

namespace onedriveapi {
namespace facets {
struct Hashes
{
  Hashes() {}
  Hashes(IJsonValue* jsonObject)
  {
    if (jsonObject)
    {
      crc32Hash = jsonObject->getKey("crc32Hash");
      sha1Hash = jsonObject->getKey("sha1Hash");
      quickXorHash = jsonObject->getKey("quickXorHash");
    }
  }
  std::string crc32Hash;
  std::string sha1Hash;
  std::string quickXorHash;
};

struct File
{
  File() {}
  File(IJsonValue* jsonObject) : hashes(jsonObject ? jsonObject->getObject("hashes").get() : nullptr)
  {
    if (jsonObject)
    {
      mimetype = jsonObject->getKey("mimeType");
      processingMetadata = jsonObject->getKeyAsBool("processingMetadata");
    }
  }
  void printData()
  {
    std::cout << "File: " << std::endl;
    std::cout << "       mimeType: " << mimetype << std::endl;
    std::cout << "       crc32Hash: " << hashes.crc32Hash << std::endl;
    std::cout << "       sha1Hash: " << hashes.sha1Hash << std::endl;
    std::cout << "       quickXorHash: " << hashes.quickXorHash << std::endl;
  }
  Hashes hashes;
  std::string mimetype;
  bool processingMetadata = false;
};

} // namespace facets
} // namespace onedriveapi
#endif // ONEDRIVEAPI_FILE_HPP
