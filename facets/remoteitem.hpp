/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    remoteitem.hpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_REMOTEITEM_HPP
#define ONEDRIVEAPI_REMOTEITEM_HPP

#include <ctime>
#include <iostream>
#include <string>

#include "../interfaces/ijsonvalue.hpp"
#include "../resources/odidentity.hpp"
#include "../resources/oditemreference.hpp"
#include "file.hpp"
#include "filesysteminfo.hpp"
#include "folder.hpp"
#include "package.hpp"
#include "shared.hpp"
#include "sharepointids.hpp"
#include "specialfolder.hpp"

namespace onedriveapi {
namespace facets {

struct RemoteItem
{
  RemoteItem() {}
  RemoteItem(IJsonValue* jsonObject)
  {
    if (jsonObject)
    {
      id = jsonObject->getKey("id");
      createdBy = createIdentitySet(jsonObject->getObject("createdBy").get());
      createdDateTime = parseTime(jsonObject->getKey("createdDateTime"));
      file = File(jsonObject->getObject("file").get());
      fileSystemInfo = FileSystemInfo(jsonObject->getObject("fileSystemInfo").get());
      folder = Folder(jsonObject->getObject("folder").get());
      lastModifiedBy = createIdentitySet(jsonObject->getObject("lastModifiedBy").get());
      lastModifiedDateTime = parseTime(jsonObject->getKey("lastModifiedDateTime"));
      name = jsonObject->getKey("name");
      package = Package(jsonObject->getObject("package").get());
      parentReference = std::make_unique<OdItemReference>(jsonObject->getObject("parentReference").get());
      shared = Shared(jsonObject->getObject("shared").get());
      sharepointIds = SharepointIds(jsonObject->getObject("sharepointIds").get());
      specialFolder = SpecialFolder(jsonObject->getObject("specialFolder").get());
      size = jsonObject->getKeyAsLong("size");
      webDavUrl = jsonObject->getKey("webDavUrl");
      webUrl = jsonObject->getKey("webUrl");
    }
  }
  void printData()
  {
    std::cout << "Remote Item:" << std::endl;
    std::cout << "=====================================" << std::endl;

    std::cout << "id:" << id << std::endl;
    std::cout << "created by: ";
    for (const auto& identity : createdBy)
    {
      identity.print();
    }
    char date[80];
    strftime(date, 80, "%Ec", &createdDateTime);
    std::cout << "created at: " << date << std::endl;
    file.printData();
    fileSystemInfo.printData();
    folder.printData();
    std::cout << "lastModifiedBy: ";
    for (const auto& identity : lastModifiedBy)
    {
      identity.print();
    }
    strftime(date, 80, "%Ec", &lastModifiedDateTime);
    std::cout << "lastModifiedDateTime: " << date << std::endl;
    std::cout << "name: " << date << std::endl;
    std::cout << "package: " << package.type << std::endl;
    parentReference->printData();
    shared.printData();
    sharepointIds.printData();
    std::cout << "specialFolder: " << specialFolder.name << std::endl;
    std::cout << "size: " << size << std::endl;
    std::cout << "webDavUrl: " << webDavUrl << std::endl;
    std::cout << "webUrl: " << webUrl << std::endl;
    std::cout << "=====================================" << std::endl;
  }
  std::string id;
  std::vector<ODIdentity> createdBy;
  std::tm createdDateTime;
  File file;
  FileSystemInfo fileSystemInfo;
  Folder folder;
  std::vector<ODIdentity> lastModifiedBy;
  std::tm lastModifiedDateTime;
  std::string name;
  Package package;
  std::shared_ptr<OdItemReference> parentReference;
  Shared shared;
  SharepointIds sharepointIds;
  SpecialFolder specialFolder;
  unsigned long size;
  std::string webDavUrl;
  std::string webUrl;
};

} // namespace facets
} // namespace onedriveapi
#endif // ONEDRIVEAPI_REMOTEITEM_HPP
