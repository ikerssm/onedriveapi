/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    folder.hpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_FOLDER_HPP
#define ONEDRIVEAPI_FOLDER_HPP

#include <iostream>
#include <string>

#include "../interfaces/ijsonvalue.hpp"

namespace onedriveapi {
namespace facets {
struct View
{
  View() {}
  View(IJsonValue* jsonObject)
  {
    if (jsonObject)
    {
      sortBy = jsonObject->getKey("sortBy");
      sortOrder = jsonObject->getKey("sortOrder");
      viewType = jsonObject->getKey("viewType");
    }
  }
  std::string sortBy;
  std::string sortOrder;
  std::string viewType;
};

struct Folder
{
  Folder() {}
  Folder(IJsonValue* folderObject) : view(folderObject ? folderObject->getObject("view").get() : nullptr)
  {
    if (folderObject)
    {
      childCount = folderObject->getKeyAsLong("childCount");
    }
  }
  void printData()
  {
    std::cout << "Folder: " << std::endl;
    std::cout << "       childCount: " << childCount << std::endl;
    std::cout << "       View: " << std::endl;
    std::cout << "             sortBy: " << view.sortBy << std::endl;
    std::cout << "             sortOrder: " << view.sortOrder << std::endl;
    std::cout << "             viewType: " << view.viewType << std::endl;
  }
  int childCount = 0;
  View view;
};

} // namespace facets
} // namespace onedriveapi
#endif // ONEDRIVEAPI_FOLDER_HPP
