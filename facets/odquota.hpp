/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    odquota.hpp
/// \date    2022/01/5
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_ODQUOTA_HPP
#define ONEDRIVEAPI_ODQUOTA_HPP

#include <iostream>
#include <string>
#include <vector>

#include "../interfaces/ijsonvalue.hpp"

namespace onedriveapi {
namespace facets {
struct ODQuota
{
  ODQuota() {}
  ODQuota(IJsonValue* jsonObject)
  {
    if (jsonObject)
    {
      total = jsonObject->getKeyAsLong("total");
      used = jsonObject->getKeyAsLong("used");
      remaining = jsonObject->getKeyAsLong("remaining");
      filecount = jsonObject->getKeyAsLong("fileCount");
      deleted = jsonObject->getKeyAsLong("deleted");
      state = jsonObject->getKey("state");
    }
  }
  void printData()
  {
    std::cout << "Quota: " << std::endl;
    std::cout << "       total: " << total << std::endl;
    std::cout << "       used: " << used << std::endl;
    std::cout << "       remaining : " << remaining << std::endl;
    std::cout << "       filecount: " << filecount << std::endl;
    std::cout << "       deleted: " << deleted << std::endl;
    std::cout << "       state: " << state << std::endl;
  }
  unsigned long total;
  unsigned long used;
  unsigned long remaining;
  unsigned long deleted;
  std::string state;
  unsigned long filecount;
};
} // namespace facets
} // namespace onedriveapi
#endif //  ONEDRIVEAPI_ODQUOTA_HPP
