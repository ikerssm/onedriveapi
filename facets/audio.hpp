/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    audio.hpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_AUDIO_HPP
#define ONEDRIVEAPI_AUDIO_HPP

#include <iostream>
#include <string>

#include "../interfaces/ijsonvalue.hpp"

namespace onedriveapi {
namespace facets {
struct Audio
{
  Audio() {}
  Audio(IJsonValue* jsonObject)
  {
    if (jsonObject)
    {
      album = jsonObject->getKey("album");
      albumArtist = jsonObject->getKey("albumArtist");
      artist = jsonObject->getKey("artist");
      bitrate = jsonObject->getKeyAsLong("bitrate");
      composers = jsonObject->getKey("composers");
      copyright = jsonObject->getKey("copyright");
      disc = jsonObject->getKeyAsLong("disc");
      discCount = jsonObject->getKeyAsLong("discCount");
      duration = jsonObject->getKeyAsLong("duration");
      genre = jsonObject->getKey("genre");
      hasDrm = jsonObject->getKeyAsBool("hasDrm");
      isVariableBitRate = jsonObject->getKeyAsBool("isVariableBitRate");
      title = jsonObject->getKey("title");
      track = jsonObject->getKeyAsLong("track");
      trackCount = jsonObject->getKeyAsLong("trackCount");
      year = jsonObject->getKeyAsLong("year");
    }
  }
  void printData()
  {
    std::cout << "Audio: " << std::endl;
    std::cout << "        album: " << album << std::endl;
    std::cout << "        albumArtist: " << albumArtist << std::endl;
    std::cout << "        artist: " << artist << std::endl;
    std::cout << "        bitrate: " << bitrate << std::endl;
    std::cout << "        composers: " << composers << std::endl;
    std::cout << "        copyright: " << copyright << std::endl;
    std::cout << "        disc: " << disc << std::endl;
    std::cout << "        discCount: " << discCount << std::endl;
    std::cout << "        duration: " << duration << std::endl;
    std::cout << "        genre: " << genre << std::endl;
    std::cout << "        hasDrm: " << hasDrm << std::endl;
    std::cout << "        isVariableBitRate: " << isVariableBitRate << std::endl;
    std::cout << "        title: " << title << std::endl;
    std::cout << "        track: " << track << std::endl;
    std::cout << "        trackCount: " << trackCount << std::endl;
    std::cout << "        year: " << year << std::endl;
  }
  std::string album;
  std::string albumArtist;
  std::string artist;
  unsigned long bitrate = 0;
  std::string composers;
  std::string copyright;
  int disc = 0;
  int discCount = 0;
  unsigned long duration = 0;
  std::string genre;
  bool hasDrm;
  bool isVariableBitRate;
  std::string title;
  int track = 0;
  int trackCount = 0;
  int year = 0;
};
} // namespace facets
} // namespace onedriveapi
#endif // AUDIO_HPP
