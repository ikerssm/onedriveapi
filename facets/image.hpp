/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    image.hpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_IMAGE_HPP
#define ONEDRIVEAPI_IMAGE_HPP

#include <iostream>

#include "../interfaces/ijsonvalue.hpp"

namespace onedriveapi {
namespace facets {
struct Image
{
  Image() {}
  Image(IJsonValue* jsonObject)
  {
    if (jsonObject)
    {
      height = jsonObject->getKeyAsLong("height");
      width = jsonObject->getKeyAsLong("width");
    }
  }
  void printData()
  {
    std::cout << "Image:" << std::endl;
    std::cout << "      width:" << width << std::endl;
    std::cout << "      height:" << height << std::endl;
  }
  int width = 0;
  int height = 0;
};

} // namespace facets
} // namespace onedriveapi
#endif // ONEDRIVEAPI_IMAGE_HPP
