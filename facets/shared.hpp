/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    shared.hpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_SHARED_HPP
#define ONEDRIVEAPI_SHARED_HPP

#include <ctime>
#include <iostream>
#include <string>

#include "../interfaces/ijsonvalue.hpp"
#include "../resources/odidentity.hpp"
#include "../templates.hpp"

namespace onedriveapi {
namespace facets {

struct Shared
{
  Shared() {}
  Shared(IJsonValue* jsonObject)
  {
    if (jsonObject)
    {
      owner = createIdentitySet(jsonObject->getObject("owner").get());
      scope = jsonObject->getKey("scope");
      sharedBy = createIdentitySet(jsonObject->getObject("sharedBy").get());
      sharedDateTime = parseTime(jsonObject->getKey("sharedDateTime"));
    }
  }
  void printData()
  {
    std::cout << "Shared: " << std::endl;
    for (const auto& identity : owner)
    {
      std::cout << "owner: ";
      identity.print();
    }
    std::cout << "     scope: " << scope << std::endl;
    for (const auto& identity : sharedBy)
    {
      std::cout << "sharedBy: ";
      identity.print();
    }
    char date[80];
    strftime(date, 80, "%Ec", &sharedDateTime);
    std::cout << "sharedDateTime: " << date << std::endl;
  }
  std::vector<ODIdentity> owner;
  std::string scope;
  std::vector<ODIdentity> sharedBy;
  std::tm sharedDateTime;
};
} // namespace facets
} // namespace onedriveapi

#endif // SHARED_HPP
