/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    sharepointids.hpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_SHAREPOINTIDS_HPP
#define ONEDRIVEAPI_SHAREPOINTIDS_HPP

#include <iostream>
#include <string>

#include "../interfaces/ijsonvalue.hpp"

namespace onedriveapi {
namespace facets {

struct SharepointIds
{
public:
  SharepointIds() {}
  SharepointIds(IJsonValue* data)
  {
    if (data)
    {
      listId = data->getKey("listId");
      listItemId = data->getKey("listItemId");
      listItemUniqueId = data->getKey("listItemUniqueId");
      siteId = data->getKey("siteId");
      siteUrl = data->getKey("siteUrl");
      tenantId = data->getKey("tenantId");
      webId = data->getKey("webId");
    }
  }
  void printData()
  {
    std::cout << "SharepointIds: " << std::endl;
    std::cout << "              listId: " << listId << std::endl;
    std::cout << "              listItemId: " << listItemId << std::endl;
    std::cout << "              listItemUniqueId : " << listItemUniqueId << std::endl;
    std::cout << "              siteId: " << siteId << std::endl;
    std::cout << "              siteUrl: " << siteUrl << std::endl;
    std::cout << "              tenantId: " << tenantId << std::endl;
    std::cout << "              webId: " << webId << std::endl;
  }
  std::string listId;
  std::string listItemId;
  std::string listItemUniqueId;
  std::string siteId;
  std::string siteUrl;
  std::string tenantId;
  std::string webId;
};
} // namespace facets
} // namespace onedriveapi
#endif // ONEDRIVEAPI_SHAREPOINTIDS_HPP
