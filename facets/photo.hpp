/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    photo.hpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_PHOTO_HPP
#define ONEDRIVEAPI_PHOTO_HPP

#include <ctime>
#include <iostream>
#include <string>

#include "../interfaces/ijsonvalue.hpp"
#include "../templates.hpp"

namespace onedriveapi {
namespace facets {

struct Photo
{
  Photo() {}
  Photo(IJsonValue* jsonObject)
  {
    if (jsonObject)
    {
      cameraMake = jsonObject->getKey("cameraMake");
      cameraModel = jsonObject->getKey("cameraModel");
      exposureDenominator = jsonObject->getKeyAsDouble("exposureDenominator");
      exposureNumerator = jsonObject->getKeyAsDouble("exposureNumerator");
      fNumber = jsonObject->getKeyAsDouble("fNumber");
      focalLength = jsonObject->getKeyAsDouble("focalLength");
      iso = jsonObject->getKeyAsLong("iso");
      takenDateTime = parseTime(jsonObject->getKey("takenDateTime"));
    }
  }
  void printData()
  {
    std::cout << "Photo:" << std::endl;
    std::cout << "          cameraMake: " << cameraMake << std::endl;
    std::cout << "          cameraModel: " << cameraModel << std::endl;
    std::cout << "          exposureDenominator: " << exposureDenominator << std::endl;
    std::cout << "          exposureNumerator: " << exposureNumerator << std::endl;
    std::cout << "          fNumber: " << fNumber << std::endl;
    std::cout << "          focalLength: " << focalLength << std::endl;
    std::cout << "          iso: " << iso << std::endl;
    char date[80];
    strftime(date, 80, "%Ec", &takenDateTime);
    std::cout << "          takenDateTime: " << date << std::endl;
  }
  std::string cameraMake;
  std::string cameraModel;
  double exposureDenominator;
  double exposureNumerator;
  double fNumber;
  double focalLength;
  int iso;
  std::tm takenDateTime;
};

} // namespace facets
} // namespace onedriveapi
#endif // ONEDRIVEAPI_PHOTO_HPP
