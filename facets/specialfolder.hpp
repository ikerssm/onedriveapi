/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    specialfolder.hpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_SPECIALFOLDER_HPP
#define ONEDRIVEAPI_SPECIALFOLDER_HPP
#include <string>

#include "../interfaces/ijsonvalue.hpp"

namespace onedriveapi {
namespace facets {

struct SpecialFolder
{
  SpecialFolder() {}
  SpecialFolder(IJsonValue* data)
  {
    if (data)
    {
      name = data->getKey("name");
    }
  }
  std::string name;
};

} // namespace facets
} // namespace onedriveapi
#endif // ONEDRIVEAPI_SPECIALFOLDER_HPP
