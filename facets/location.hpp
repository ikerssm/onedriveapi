/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    location.hpp
/// \date    2022/01/12
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_LOCATION_HPP
#define ONEDRIVEAPI_LOCATION_HPP

#include <iostream>

#include "../interfaces/ijsonvalue.hpp"

namespace onedriveapi {
namespace facets {

struct Location
{
  Location() {}
  Location(IJsonValue* jsonObject)
  {
    if (jsonObject)
    {
      latitude = jsonObject->getKeyAsDouble("latitude");
      longitude = jsonObject->getKeyAsDouble("longitude");
      altitude = jsonObject->getKeyAsDouble("altitude");
    }
  }
  void printData()
  {
    std::cout << "Location:" << std::endl;
    std::cout << "          altitude:" << altitude << std::endl;
    std::cout << "          latitude:" << latitude << std::endl;
    std::cout << "          longitude:" << longitude << std::endl;
  }
  double altitude;
  double latitude;
  double longitude;
};

} // namespace facets
} // namespace onedriveapi
#endif // ONEDRIVEAPI_LOCATION_HPP
