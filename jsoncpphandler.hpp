/// OneDriveApi
/// Copyright (C) 2022, Iker Salmon San Millan
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program.  If not, see <http://www.gnu.org/licenses/>..
///
/// \file    jsoncppparser.hpp
/// \date    2022/01/4
/// \author  Iker Salmon San Millan
///
#ifndef ONEDRIVEAPI_JSONCPPHANDLER_HPP
#define ONEDRIVEAPI_JSONCPPHANDLER_HPP

#include "onedriverestapi.hpp"

#include <jsoncpp/json/json.h>

namespace onedriveapi {

class JsonCppHandler : public IJsonValue
{
public:
  JsonCppHandler();
  void setValue(const Json::Value& value);

  // JsonHandlerInterface interface
public:
  const string getKey(const string& key) override;
  const long getKeyAsLong(const string& key) override;
  const bool getKeyAsBool(const std::string& key) override;
  const bool getKeyAsDouble(const std::string& key) override;
  bool parseJson(const std::string& jsonString) override;
  const std::shared_ptr<IJsonValue> getObject(const string& key) override;
  const std::vector<std::shared_ptr<IJsonValue>> getObjects(const string& key) override;
  const string& jsonAsString() override;
  const std::vector<std::string> getKeys() override;

private:
  Json::Value mValue;
};
} // namespace onedriveapi

#endif // ONEDRIVEAPI_JSONCPPPARSER_HPP
